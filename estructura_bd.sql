create table sis_usuario(
    id int auto_increment primary key,
    nombre varchar(200),
    login varchar(100),
    email varchar(200),
    password varchar(100),
	rnt varchar(50),
	representante varchar(200),
	ciudad varchar(200),
    admin char(1) default 'N',
    activo char(1) default 'Y',
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_grupo(
    id int auto_increment primary key,
    nivel1 int,
    nivel2 int,
    nivel3 int,
    nivel4 int,
    nivel5 int,
    nombre varchar(200),
    descripcion text,
    orden int,
    id_grupo int default null,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_grupo) references sis_grupo (id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_pregunta(
    id int auto_increment primary key,
    enunciado text,
    id_grupo int,
    orden int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_grupo) references sis_grupo (id)    
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_opcion_pregunta(
    id int auto_increment primary key,
    nombre varchar(200),
    valor int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_respuesta(
    id int auto_increment primary key,
    id_usuario int,
    id_pregunta int,
    id_opcion int,
    observaciones text,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_usuario) references sis_usuario (id),
    foreign key (id_pregunta) references sis_pregunta (id),
    foreign key (id_opcion) references sis_opcion_pregunta (id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_respuesta_mensaje(
    id int auto_increment primary key,
    id_pregunta int,
    id_opcion int,
    mensaje text,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_pregunta) references sis_pregunta (id),
    foreign key (id_opcion) references sis_opcion_pregunta (id)    
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_plan_accion(
    id int auto_increment primary key,
    id_usuario int,
    id_pregunta int,
    accion text,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_usuario) references sis_usuario (id),
    foreign key (id_pregunta) references sis_pregunta (id)    
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE sis_pregunta_map(
	nivel1 INT,
	id_grupo INT NOT NULL DEFAULT '0',
	id_pregunta INT DEFAULT '0',
	mostrar_grupo INT,
	grupo_ant INT NOT NULL DEFAULT '0',
	peso INT NOT NULL DEFAULT '0',
	cnt int, 
	grupo int,
	cambio int
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE sis_pregunta_nivel_map (
	nivel1 INT NULL DEFAULT NULL,
	grupo_inicio int,
	grupo_fin int
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
