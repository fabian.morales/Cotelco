@extends('master')

@section('content')

@section('js_header')
<script>
(function (window, $){
    $(document).ready(function() {
        
        tinymce.init({
            selector: "textarea#descripcion",
            height: 300,
            theme: 'modern',
            language: 'es',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste textcolor"
            ],
            toolbar: "undo redo | cut copy paste | styleselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist charmap",
            menubar: false
        });
        
        /*$("#formComentario").validate({ 
            messages: {	
                calificacion: "Debe seleccionar la calificación",
                contenido: "Debe escribir el comentario"
            },
            errorLabelContainer: "#divErrores",
            wrapper: "li",
            invalidHandler: function(event, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $("#divErrores").html("<p>Aviso</p><p>No se guardar el comentario debido a estos errores:</p>");
                    $("#divErrores").addClass("danger alert");
                    $("#divErrores").show();
                } 
                else {
                    $("#divErrores").removeClass("danger alert");
                    $("#divErrores").hide();
                }
            }
        });*/
    });
})(window, jQuery);
</script>
@stop
<div class="row">
    <div class="large-12 columns">
        <h2>Datos del grupo</h2>
        <form id="form_grupo" name="form_grupo" action="{{ url('grupo/guardar') }}" method="post">
            <input type="hidden" id="id" name="id" value="{{ $grupo->id }}" />
                        
            <ul class="tabs" data-tab>
              <li class="tab-title active"><a href="#panelBasico">Datos b&aacute;sicos</a></li>
              <li class="tab-title"><a href="#panelPreguntas">Preguntas</a></li>
            </ul>
            <div class="tabs-content">
                <div class="content active" id="panelBasico">
                    <div class="row">
                        <div class="medium-4 small-12 columns">
                            <label for="numeral">Nivel</label>
                        </div>
                        <div class="medium-8 small-12 columns">
                            <div class="row">
                                <div class="small-2 columns"><input type="text" name="nivel1" id="nivel1" value="{{ $grupo->nivel1 }}" /></div>
                                <div class="small-2 columns"><input type="text" name="nivel2" id="nivel2" value="{{ $grupo->nivel2 }}" /></div>
                                <div class="small-2 columns"><input type="text" name="nivel3" id="nivel3" value="{{ $grupo->nivel3 }}" /></div>
                                <div class="small-2 columns"><input type="text" name="nivel4" id="nivel4" value="{{ $grupo->nivel4 }}" /></div>
                                <div class="small-2 columns end"><input type="text" name="nivel5" id="nivel5" value="{{ $grupo->nivel5 }}" /></div>
                            </div>                    
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-4 small-12 columns">
                            <label for="nombre">Nombre</label>
                        </div>
                        <div class="medium-8 small-12 columns">
                            <input type="text" name="nombre" id="nombre" value="{{ $grupo->nombre }}" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-4 small-12 columns end">
                            <label for="descripcion">Descripcion</label>
                        </div>
                        <div class="small-12 columns">
                            <textarea name="descripcion" id="descripcion">{{ $grupo->descripcion }}</textarea>
                            <br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-4 small-12 columns">
                            <label for="id_grupo">Grupo padre</label>
                        </div>
                        <div class="medium-8 small-12 columns">
                            <select id="id_grupo" name="id_grupo">
                                <option></option>
                                @foreach($grupos as $g)
                                <option value="{{ $g->id }}" @if($g->id == $grupo->id_grupo)selected@endif>{{ $g->mostrarNivel() }} {{ $g->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="content" id="panelPreguntas">
                  @include('grupo.lista_preguntas', array("grupo" => $grupo))
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <input type="submit" value="Guardar" class="button default" />
                    <a href="{{ url('/grupo/') }}" class="button alert medium">Regresar</a>
                </div>
            </div>              
        </form>
    </div>
</div>

@stop