@extends('master')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3>Lista de grupos</h3>
    </div>
</div>
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('grupo/crear') }}" class="button">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
@include('grupo.lista', array("grupos" => $grupos))
@stop