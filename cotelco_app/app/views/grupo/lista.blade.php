<div class="row">
    <div class="small-12 columns">
        <table>
            <thead>
                <tr>
                    <th>Consecutivo</th>
                    <th>Nivel</th>                    
                    <th>Nombre</th>                    
                    <th>Editar</th>                    
                </tr>
            </thead>
            <tbody>
                @foreach($grupos as $g)
                <tr>
                    <td>{{ $g->id }}</td>
                    <td>
                        @if($g->nivel1){{ $g->nivel1 }}.@endif
                        @if($g->nivel2){{ $g->nivel2 }}.@endif
                        @if($g->nivel3){{ $g->nivel3 }}.@endif
                        @if($g->nivel4){{ $g->nivel4 }}.@endif
                        @if($g->nivel5){{ $g->nivel5 }}@endif
                    </td>                 
                    <td>{{ $g->nombre }}</td>                    
                    <td><a href="{{ url('/grupo/editar/'.$g->id) }}"><i class="fi-pencil"></i></a></td>                    
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('/grupo/reconstruir') }}" class="button alert">Reconstruir estructura de grupos de preguntas</a>
    </div>
</div>
<div class="row">
    <div class="small-12 columns text-center">
        {{ $grupos->links() }}
    </div>
</div>


