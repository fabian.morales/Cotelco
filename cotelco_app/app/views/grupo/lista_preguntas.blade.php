<div class="row">
    <div class="small-12 columns">
        <h4>Lista de preguntas asignadas</h4>
    </div>
</div>
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('pregunta/crear/'.$grupo->id) }}" class="button small">Nueva pregunta <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row">
    <div class="small-12 columns">
        <table>
            <thead>
                <tr>
                    <th>Consecutivo</th>
                    <th>Enunciado</th>                    
                    <th>Orden</th>                    
                    <th>Editar</th>                    
                </tr>
            </thead>
            <tbody>
                @foreach($grupo->preguntas as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->recortarEnunciado() }}</td>                 
                    <td>{{ $p->orden }}</td>                    
                    <td><a href="{{ url('/pregunta/editar/'.$p->id) }}"><i class="fi-pencil"></i></a></td>                    
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
