@extends('master')

@section('content')

@section('js_header')
<script>
(function (window, $){
    $(document).ready(function() {
        
        tinymce.init({
            selector: "textarea#enunciado",
            height: 300,
            theme: 'modern',
            language: 'es',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste textcolor"
            ],
            toolbar: "undo redo | cut copy paste | styleselect | forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist charmap",
            menubar: false
        });
        
        /*$("#formComentario").validate({ 
            messages: {	
                calificacion: "Debe seleccionar la calificación",
                contenido: "Debe escribir el comentario"
            },
            errorLabelContainer: "#divErrores",
            wrapper: "li",
            invalidHandler: function(event, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $("#divErrores").html("<p>Aviso</p><p>No se guardar el comentario debido a estos errores:</p>");
                    $("#divErrores").addClass("danger alert");
                    $("#divErrores").show();
                } 
                else {
                    $("#divErrores").removeClass("danger alert");
                    $("#divErrores").hide();
                }
            }
        });*/
    });
})(window, jQuery);
</script>
@stop
<div class="row">
    <div class="large-12 columns">
        <h2>Datos de la pregunta</h2>
        <form id="form_pregunta" name="form_pregunta" action="{{ url('pregunta/guardar') }}" method="post">
            <input type="hidden" id="id" name="id" value="{{ $pregunta->id }}" />
            <input type="hidden" id="id_grupo" name="id_grupo" value="{{ $pregunta->id_grupo }}" />
            <div class="row">
                <div class="medium-4 small-12 columns end">
                    <label for="enunciado">Enunciado</label>
                </div>
                <div class="small-12 columns">
                    <textarea name="enunciado" id="enunciado">{{ $pregunta->enunciado }}</textarea>
                    <br />
                </div>
            </div>
            <div class="row">
                <div class="medium-4 small-12 columns">
                    <label for="Orden">Orden</label>
                </div>
                <div class="medium-8 small-12 columns">
                    <input type="text" name="orden" id="orden" value="{{ $pregunta->orden }}" />
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <h4>Mensajes de acci&oacute;n</h4>
                </div>
            </div>
            
            @foreach($opciones as $o)
            <div class="row">
                <div class="medium-4 small-12 columns">
                    <label for="opcion_{{ $o->id }}">{{ $o->nombre }}</label>
                </div>
                <div class="medium-8 small-12 columns">
                    <input type="text" name="opcion[{{ $o->id }}]" id="opcion_{{ $o->id }}" value="@if(sizeof($o->mensajes->first())){{ $o->mensajes->first()->mensaje }}@endif" />
                </div>
            </div>
            @endforeach
            
            <div class="row">
                <div class="small-12 columns">
                    <input type="submit" value="Guardar" class="button medium" />                    
                    <a href="{{ url('/grupo/editar/'.$pregunta->id_grupo) }}" class="button alert medium">Regresar</a>
                </div>
            </div>              
        </form>
    </div>
</div>

@stop