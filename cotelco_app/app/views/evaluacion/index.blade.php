@extends('master')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3>Bienvenido {{ Auth::user()->nombre }}</h3>
        <h4 class="subheader">&Iacute;ndice de la evaluaci&oacute;n</h4>
    </div>
</div>

@foreach($grupos as $g)
<div class="row">
    <div class="small-12 columns">        
        @if(empty($g->nivel2))
            <br />
            <h5 class="nivel1 uppercase"><a href="{{ url('/evaluacion/preguntas/'.$g->id) }}">{{ $g->mostrarNivel() }} {{ $g->nombre }}</a></h5>
        @elseif(empty($g->nivel3))
            <h6 class="nivel2"><a href="{{ url('/evaluacion/preguntas/'.$g->id) }}">{{ $g->mostrarNivel() }} {{ $g->nombre }}</a></h6>
        @elseif(empty($g->nivel4))
            <h6 class="nivel3 subheader"><a href="{{ url('/evaluacion/preguntas/'.$g->id) }}">{{ $g->mostrarNivel() }} {{ $g->nombre }}</a></h6>
        @else
            <h6 class="niveln subheader"><a href="{{ url('/evaluacion/preguntas/'.$g->id) }}">{{ $g->mostrarNivel() }} {{ $g->nombre }}</a></h6>
        @endif
    </div>
</div>
@endforeach
@stop