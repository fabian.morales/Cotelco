@extends('master')

@section('content')
{{--*/ $mostrarEncabezado = true /*--}}
<div class="seccion seccion_{{ $items[0]->nivel1 }}">
    <div id="anexos">
        <a href="{{ asset('pdf/anexos_nts_ts_002_alojamiento_v2.pdf') }}" target="_blank"><img src="{{ asset('img/anexo.png') }}" /></a>
    </div>
    <div class="separador2"></div>
    <form id="form_preguntas" method="post" action="{{ url('/evaluacion/guardar') }}">
    <div class="row fullWidth">
        <div class="small-1 columns">&nbsp;</div>
        <div class="small-8 columns end">
            @foreach($items as $i)
            @if($i->mostrar_grupo == 1)
                <div class="row">
                    <div class="small-12 columns">
                        @if(empty($i->grupo->nivel2))
                        <img src="{{ asset('img/seccion_'.$i->nivel1.'/titulo.png') }}" />
                        <!--div class="row">
                            <div class="small-5 columns">
                                <img src="{{ asset('img/seccion_'.$i->nivel1.'/titulo.png') }}" />
                            </div>
                            <div class="small-7 columns">
                                @if(!empty($i->grupo->descripcion))
                                <div class="info_burbuja">
                                    {{ $i->grupo->descripcion }}
                                </div>
                                @endif
                            </div>
                        </div-->
                        @else
                        <div class="titulo_grupo">{{ $i->grupo->mostrarNivel() }} {{ $i->grupo->nombre }}</div>
                        <div class="descripcion_grupo">{{ $i->grupo->descripcion }}</div>
                        @endif
                    </div>
                </div>
            @endif

            @if(sizeof($i->pregunta))
                @if ($mostrarEncabezado)
                <div class="row">
                    <div class="small-6 columns">&nbsp;</div>
                    <div class="small-6 columns">
                        <div class="row">
                            @foreach($opciones as $o)
                                <div class="small-2 columns encabezado_opc">{{ $o->nombre }}</div>
                            @endforeach
                            <div class="small-6 columns encabezado_opc">Tarea</div>
                        </div>
                    </div>
                </div>
                {{--*/ $mostrarEncabezado = false /*--}}
                @endif
                <div class="row">
                    <div class="small-6 columns"><div class="enunciado_pregunta">{{ $i->pregunta->enunciado }}</div></div>
                    <div class="small-6 columns">
                        <div class="row">
                            @foreach($opciones as $o)
                                <div class="small-2 columns espacio_opc">
                                    <input type="radio" id="opc_{{ $i->pregunta->id }}_{{ $o->id }}" name="opc[{{ $i->pregunta->id }}]" value="{{ $o->id }}" @if(sizeof($i->pregunta->respuestas) && $i->pregunta->respuestas[0]->id_opcion == $o->id)checked@endif />
                                </div>
                            @endforeach
                            <div class="small-6 columns espacio_opc">
                                <input type="text" id="tarea_{{ $i->pregunta->id }}" name="tarea[{{ $i->pregunta->id }}]" value="@if(sizeof($i->pregunta->respuestas)){{ $i->pregunta->respuestas[0]->observaciones }}@endif" />
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @endforeach
        </div>
    </div>
    <div class="row fullWidth collapse">
        <div class="small-8 columns small-offset-1 end">
            {{--*/ $linkAnt = PreguntaMap::generarLink($items[0]->pagina, "ant") /*--}}
            {{--*/ $linkSig = PreguntaMap::generarLink($items[0]->pagina, "sig") /*--}}
            
            @if(!empty($linkAnt))
            <a class="boton_ant left" id="lnkAnterior" href="{{ $linkAnt }}">&nbsp;</a>
            @endif
            @if(!empty($linkSig))
            <a class="boton_sig right" id="lnkSiguiente" href="{{ $linkSig }}">&nbsp;</a>
            @endif
        </div>
    </div>
    </form>
    <div class="row fullWidth collapse marcadores">
        <div class="small-12 columns">
            @foreach($grupos as $g)
            <a class="marcador left" href="{{ url('/evaluacion/preguntas/'.$g->preguntaNivelMap->pagina_inicio) }}"><img src="{{ asset('img/seccion_'.$g->nivel1.'/marcador.png') }}" /></a>
            @endforeach
        </div>
        <div class="small-12 columns">
            NOTA: La numeración de los requisitos corresponde a los definidos en la norma
        </div>
    </div>
</div>
@stop