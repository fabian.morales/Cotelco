<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<style>
@import url(//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700);
body{
    font-family: 'Open Sans', Helvetica, Arial, sans-serif;
    font-size: 13px;
    margin: 0;
    padding: 0;
}

table{
    width: 100%;
}

tr.linea td{
    margin-bottom: 5px;
    /*border-bottom: 1px solid #ccc;*/
}

tr.linea_enc td{
    /*border-bottom: 2px solid #999;*/
    margin-bottom: 10px;
}


div, p{
    margin: 0;
    padding: 0;
}
</style>
</head>
<body>
<table style="width: 100%" cellspacing="0" cellpadding="0">
    <tr>
        <td colspan="3">
            <h1 style="text-align: center">AUTOEVALUACI&Oacute;N NTS TS 002</h1>
            <h2 style="text-align: center">Hotel: {{ $usuario->nombre }}</h2>
        </td>
    </tr>
    <tr>
        <td style="width: 60%;"></td>
        <td style="width: 10%;"></td>
        <td style="width: 30%;"></td>
    </tr>
    @foreach($grupos as $g)
        <tr class="linea">
            <td><strong>{{ $g->mostrarNivel() }}.  {{ $g->nombre }}</strong></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        @if(sizeof($g->preguntas))
            <tr class="linea_enc">
                <td><strong>Pregunta</strong></td>
                <td>&nbsp;</td>
                <td><strong>Tarea</strong></td>
            </tr>
            @foreach($g->preguntas as $p)
            <tr class="linea">
                <td valign="top">{{ str_replace(array("<p>", "</p>"), array("<div> - ", "</div><br />"), $p->enunciado) }}</td>
                <td>&nbsp;</td>
                <td valign="top">@if(sizeof($p->respuestas)) {{ trim($p->respuestas[0]->observaciones) }} @endif</td>
            </tr>
            @endforeach
        @endif
    @endforeach
    
</table>
</body>
</html>