@extends('master')

@section('js_header')
<!--script>
(function($, window){
    $(document).ready(function() {
        $('#agradecimiento').foundation('reveal','open');
    });
})(jQuery, window);
</script-->
@stop

@section('content')
<div class="seccion seccion_{{ $grupo->nivel1 }}">
    <div id="anexos">
        <a href="{{ asset('pdf/anexos_nts_ts_002_alojamiento_v2.pdf') }}" target="_blank"><img src="{{ asset('img/anexo.png') }}" /></a>
    </div>
    @if(!sizeof($grupoSig))
    <div class="row">
        <div class="small-12 columns reveal-modal" id='agradecimiento' data-reveal>
            <div class="row">
                <div class="small-12 columns text-center">
                    <!--img src="{{ asset('img/gracias.png') }}" />
                    <br /-->
                    <div style="font-weight: bold; font-size: 1.5rem; margin-bottom: 5px">Gracias por utilizar la herramienta de autoevaluaci&oacute;n</div>
                    <div style="font-weight: bold; font-size: 1.5rem; margin-bottom: 15px">Para mayor informaci&oacute;n:</div>
                </div>
            </div>
            <div class="row">
                <div class="small-8 columns small-centered">
                    <div class="row">
                        <div class="small-6 columns text-right"><strong>Contacto</strong></div>
                        <div class="small-6 columns" style="padding-left: 5px; border-left: 2px solid #000">
                            <div><strong>Carrera 11 # 69-79</strong></div>
                            <div><strong>(57-1) 7427766</strong></div>
                            <div><strong>(57-1) 7427765</strong></div>
                            <div style="color: #11a8a8"><strong>Bogot&aacute;, Distrito Capital</strong></div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="close-reveal-modal" aria-label="Cerrar">&#215;</a>
        </div>
    </div>
    @endif
    
    <div class="row fullWidth">
        <div class="small-10 end columns box">
            <div class="row">
                <div class="small-2 columns text-right"><img src="{{ asset('img/seccion_'.$grupo->nivel1.'/pin.png') }}" /></div>
                <div class="small-4 columns text-center">
                    <img src="{{ asset('img/seccion_'.$grupo->nivel1.'/titulo.png') }}" />
                </div>
                <div class="small-6 columns text-center">
                    <a class="boton_tran" href="{{ url('/evaluacion/grafico/'.$grupo->nivel1) }}">
                        <img src="{{ asset('img/seccion_'.$grupo->nivel1.'/graficos.png') }}" />
                    </a>
                    <a class="boton_tran" target="_blank" href="{{ url('/evaluacion/reporteParcial/'.$grupo->nivel1) }}">
                        <img src="{{ asset('img/seccion_'.$grupo->nivel1.'/resultados.png') }}" />
                    </a>
                    <a class="boton_tran" target="_blank" href="{{ url('/evaluacion/tareas/'.$grupo->nivel1) }}">
                        <img src="{{ asset('img/seccion_'.$grupo->nivel1.'/tareas.png') }}" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row fullWidth">
        <div class="small-10 end columns box">
            <div class="row">
                @if(sizeof($grupoSig))
                <div class="small-2 columns text-right">
                    <img src="{{ asset('img/seccion_'.$grupoSig->nivel1.'/pin.png') }}" />
                </div>
                <div class="small-4 columns text-center end">
                    <img src="{{ asset('img/seccion_'.$grupoSig->nivel1.'/titulo.png') }}" />
                </div>
                <div class="small-6 columns text-center">
                    <a class="boton_tran" href="{{ url('/evaluacion/preguntas/'.$grupoSig->preguntaNivelMap->pagina_inicio) }}">
                        <img src="{{ asset('img/seccion_'.$grupoSig->nivel1.'/continuar.png') }}" />
                    </a>
                </div>
                @else
                <div class="small-2 columns text-right">
                    <img src="{{ asset('img/seccion_final/pin.png') }}" />
                </div>
                <div class="small-4 columns text-center">
                    <img src="{{ asset('img/seccion_final/titulo.png') }}" />
                </div>
                <div class="small-6 columns text-center">
                    <a class="boton_tran" href="{{ url('/evaluacion/graficoFinal') }}">
                        <img src="{{ asset('img/seccion_final/graficos.png') }}" />
                    </a>
                    <a class="boton_tran" target="_blank" href="{{ url('/evaluacion/reporte') }}">
                        <img src="{{ asset('img/seccion_final/resultados.png') }}" />
                    </a>
                    <a class="boton_tran" target="_blank" href="{{ url('/evaluacion/tareasFinal') }}">
                        <img src="{{ asset('img/seccion_final/tareas.png') }}" />
                    </a>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row fullWidth collapse">
        <div class="small-8 columns small-offset-1 end">
            <a class="boton_ant left" href="{{ url('/evaluacion/preguntas/'.$grupo->preguntaNivelMap->pagina_fin) }}">&nbsp;</a>
            @if(sizeof($grupoSig))
            <a class="boton_sig right" href="{{ url('/evaluacion/preguntas/'.$grupoSig->preguntaNivelMap->pagina_inicio) }}">&nbsp;</a>
            @endif
        </div>
    </div>    
    <div class="row fullWidth collapse marcadores">
        <div class="small-12 columns">
            @foreach($grupos as $g)
            <a class="marcador left" href="{{ url('/evaluacion/preguntas/'.$g->preguntaNivelMap->pagina_inicio) }}"><img src="{{ asset('img/seccion_'.$g->nivel1.'/marcador.png') }}" /></a>
            @endforeach
        </div>
        <div class="small-12 columns">
            NOTA: La numeración de los requisitos corresponde a los definidos en la norma
        </div>
    </div>
</div>
@stop