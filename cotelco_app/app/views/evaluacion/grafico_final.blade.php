@extends('master')
@section ('js_header')
<script src="{{ asset('js/chart/Chart.js') }}"></script>
<script>
(function($, window){
    $(document).ready(function() {
        var colores1 = {
            3: "rgba(140,198,63,0.7)",
            4: "rgba(46,175,86,0.7)",
            5: "rgba(17,168,168,0.7)",
            6: "rgba(251,176,59,0.7)"
        };
        
        var colores2 = {
            3: "rgba(140,198,63,0.9)",
            4: "rgba(46,175,86,0.9)",
            5: "rgba(17,168,168,0.9)",
            6: "rgba(251,176,59,0.9)"
        };
        
        var opciones = {    
            scaleBeginAtZero : true,
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.2)",
            scaleGridLineWidth : 1,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: true,
            barShowStroke : true,
            barStrokeWidth : 2,
            barValueSpacing : 5,
            barDatasetSpacing : 1,
            scaleLineColor: "rgba(0,0,0,.3)",
            scaleLineWidth: 2,
            scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
            scaleFontSize: 14,
            scaleFontStyle: "bold",
            scaleFontColor: "#111"
        };
        
        @foreach($grupos as $g)
        //{{--*/ $resultados = $g->obtenerDatosGrafico($usuario->id) /*--}}
        
        var datos{{ $g->id }} = {
            labels: [@foreach($resultados as $i => $r)"{{ $r->numeral }}"@if($i < count($resultados) - 1),@endif@endforeach],
            datasets: [
                {
                    label: "{{ $g->nombre }}",
                    fillColor: colores1[{{ $g->nivel1 }}],
                    strokeColor: "rgba(220,220,220,0.8)",
                    highlightFill: colores2[{{ $g->nivel1 }}],
                    highlightStroke: "rgba(220,220,220,1)",
                    data: [@foreach($resultados as $i => $r)"{{ $r->cumplimiento }}"@if($i < count($resultados) - 1),@endif@endforeach]
                }
            ]
        };
        
        var ctx{{ $g->id }} = $("#canvas_grafico{{ $g->id }}").get(0).getContext("2d");
        var myBarChart{{ $g->id }} = new Chart(ctx{{ $g->id }}).Bar(datos{{ $g->id }}, opciones);
        @endforeach        
    });
})(jQuery, window);
</script>
@stop

@section('content')
<div class="seccion seccion_final">
    <div id="anexos">
        <a href="{{ asset('pdf/anexos_nts_ts_002_alojamiento_v2.pdf') }}" target="_blank"><img src="{{ asset('img/anexo.png') }}" /></a>
    </div>
    <div class="separador2"></div>
    <div class="row">
        @foreach($grupos as $g)
        <div class="small-6 columns div_grafico">
            <img src="{{ asset('img/seccion_'.$g->nivel1.'/titulo.png') }}" />
            <br />
            <br />
            <canvas id="canvas_grafico{{ $g->id }}" width="400" height="350px"></canvas>
        </div>
        @endforeach
    </div>
    <div class="row">
        <div class="small-10 columns end box">
            <strong>Hotel: </strong>{{ $usuario->nombre }}<br />
            <strong>RNT: </strong>{{ $usuario->rnt }}
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <a class="boton_ant_res left" href="{{ url('/evaluacion/transicion/'.$grupos->last()->nivel1) }}">&nbsp;</a>
        </div>
    </div>
    <br />
    <br />
    <br />
    <div class="row fullWidth collapse marcadores">
        <div class="small-12 columns">
            @foreach($gruposMap as $g)
            <a class="marcador left" href="{{ url('/evaluacion/preguntas/'.$g->preguntaNivelMap->pagina_inicio) }}"><img src="{{ asset('img/seccion_'.$g->nivel1.'/marcador.png') }}" /></a>
            @endforeach
        </div>
        <div class="small-12 columns">
            NOTA: La numeración de los requisitos corresponde a los definidos en la norma
        </div>
    </div>
</div>
@stop