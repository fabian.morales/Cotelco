@extends('master')
@section ('js_header')
<script src="{{ asset('js/chart/Chart.js') }}"></script>
<script>
(function($, window){
    $(document).ready(function() {
        var colores1 = {
            3: "rgba(140,198,63,0.7)",
            4: "rgba(46,175,86,0.7)",
            5: "rgba(17,168,168,0.7)",
            6: "rgba(251,176,59,0.7)"
        };
        
        var colores2 = {
            3: "rgba(140,198,63,0.9)",
            4: "rgba(46,175,86,0.9)",
            5: "rgba(17,168,168,0.9)",
            6: "rgba(251,176,59,0.9)"
        };
        
        var datos = {
            labels: [@foreach($resultados as $i => $r)"{{ $r->numeral }}"@if($i < count($resultados) - 1),@endif@endforeach],
            datasets: [
                {
                    label: "{{ $grupo->nombre }}",
                    fillColor: colores1[{{ $grupo->nivel1 }}],
                    strokeColor: "rgba(220,220,220,0.8)",
                    highlightFill: colores2[{{ $grupo->nivel1 }}],
                    highlightStroke: "rgba(220,220,220,1)",
                    data: [@foreach($resultados as $i => $r)"{{ $r->cumplimiento }}"@if($i < count($resultados) - 1),@endif@endforeach]
                }
            ]
        };

        var opciones = {    
            scaleBeginAtZero : true,
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.2)",
            scaleGridLineWidth : 1,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: true,
            barShowStroke : true,
            barStrokeWidth : 2,
            barValueSpacing : 5,
            barDatasetSpacing : 1,
            scaleLineColor: "rgba(0,0,0,.3)",
            scaleLineWidth: 2,
            scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
            scaleFontSize: 14,
            scaleFontStyle: "bold",
            scaleFontColor: "#111"
        };

        var ctx = $("#canvas_grafico").get(0).getContext("2d");
        var myBarChart = new Chart(ctx).Bar(datos, opciones);
    });
})(jQuery, window);
</script>
@stop

@section('content')
<div class="seccion seccion_{{ $grupo->nivel1 }}">
    <div id="anexos">
        <a href="{{ asset('pdf/anexos_nts_ts_002_alojamiento_v2.pdf') }}" target="_blank"><img src="{{ asset('img/anexo.png') }}" /></a>
    </div>
    <div class="separador2"></div>
    <div class="row fullWidth">
        <div class="small-8 small-offset-1 columns">
            <div class="row">
                <div class="small-12 columns"><img src="{{ asset('img/seccion_'.$grupo->nivel1.'/titulo.png') }}" /></div>
                <br />
                <br />
            </div>
            <div class="row">
                <div class="small-12 columns"><canvas id="canvas_grafico" width="550" height="400"></canvas></div>
            </div>
            <div class="row">
                <div class="small-10 columns end box">
                    <strong>Hotel: </strong>{{ $usuario->nombre }}<br />
                    <strong>RNT: </strong>{{ $usuario->rnt }}
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <a class="boton_ant_res left" href="{{ url('/evaluacion/transicion/'.$grupo->nivel1) }}">&nbsp;</a>
                </div>
            </div>
            <br />
            <br />
            <br />
        </div>
        <div class="small-3 columns grupos_home">
            @foreach($gruposNiv as $g)
            <div><strong>{{ $g->mostrarNivel() }}. {{ $g->nombre }}</strong></div>
            <br />
            @endforeach
        </div> 
    </div>
    <div class="row fullWidth collapse marcadores">
        <div class="small-12 columns">
            @foreach($grupos as $g)
            <a class="marcador left" href="{{ url('/evaluacion/preguntas/'.$g->preguntaNivelMap->pagina_inicio) }}"><img src="{{ asset('img/seccion_'.$g->nivel1.'/marcador.png') }}" /></a>
            @endforeach
        </div>
        <div class="small-12 columns">
            NOTA: La numeración de los requisitos corresponde a los definidos en la norma
        </div>
    </div>
</div>
@stop