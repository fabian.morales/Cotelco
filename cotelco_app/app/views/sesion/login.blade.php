@extends('master')

@section('content')
<div class="row">
    <div class="small-12 medium-7 large-6 columns small-centered medium-centered large-centered">
        <h3 class="text-center">Formulario inicio sesi&oacute;n</h3>
        <form class="formulario" id="form_login" name="form_login" action="{{ url('/sesion/login') }}" method="post">
            <div class="row">
                <div class="small-12 columns">
                    NIT
                </div>
                <div class="small-12 columns">
                    <input type="text" name="login" id="login" placeholder="" />    
                </div>
            </div>
            <div class="row">
                <div class="small-2 columns">
                    Clave
                </div>
                <div class="small-12 columns">
                    <input type="password" name="password" id="password" placeholder="" />
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <input type="submit" value="" class="boton_sig right" />
                </div>
            </div>
        </form>
    </div>
</div>
@stop