@extends('master')

@section('content')
<div class="row">
    <div class="small-12 medium-7 large-6 columns small-centered medium-centered large-centered">
        <h3 class="text-center">Formulario de ayuda</h3>
        <form class="formulario" id="form_ayuda" name="form_ayuda" action="{{ url('/sesion/ayuda/enviar') }}" method="post">
            <div class="row">
                <div class="small-12 columns">
                    Nombre del hotel
                </div>
                <div class="small-12 columns">
                    <input type="text" name="nombre" id="nombre" value="{{ $usuario->nombre }}" readonly />    
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    Nombre de quien realiza la autoevaluaci&oacute;n
                </div>
                <div class="small-12 columns">
                    <input type="text" name="nombre_repr" id="nombre_repr" value="{{ $usuario->nombre_ins }}" readonly />    
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    RNT
                </div>
                <div class="small-12 columns">
                    <input type="text" name="rnt" id="rnt" value="{{ $usuario->rnt }}" readonly />    
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    Direcci&oacute;n
                </div>
                <div class="small-12 columns">
                    <input type="text" name="direccion" id="direccion" value="{{ $usuario->direccion_ins }}" readonly />    
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    Tel&eacute;fono
                </div>
                <div class="small-12 columns">
                    <input type="text" name="telefono" id="telefono" value="{{ $usuario->telefono_ins }}" readonly />    
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    Ciudad
                </div>
                <div class="small-12 columns">
                    <input type="text" name="ciudad" id="ciudad" value="{{ $usuario->ciudad_ins }}" readonly />    
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">Correo</div>
                <div class="small-12 columns">
                    <input type="email" name="correo_repr" id="correo_repr" value="{{ $usuario->correo_ins }}" placeholder="" readonly />
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">Comentarios</div>
                <div class="small-12 columns">
                    <textarea name="comentarios" id="comentarios" required></textarea>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <input type="submit" value="" class="boton_sig right" />
                </div>
            </div>
        </form>
    </div>
</div>
@stop