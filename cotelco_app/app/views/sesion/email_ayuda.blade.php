<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Solicitud de ayuda</h2>
		
		<div>El hotel <strong>{{ $usuario->nombre }}</strong> ha realizado una solicitud de ayuda con los siguientes datos:</div>
        <ul>
            <li><strong>Representante legal: </strong> {{ $usuario->nombre_ins }}</li>
            <li><strong>RNT: </strong> {{ $usuario->rnt }}</li>
            <li><strong>Direcci&oacute;n: </strong> {{ $usuario->direccion_ins }}</li>
            <li><strong>Tel&eacute;fono: </strong> {{ $usuario->telefono_ins }}</li>
            <li><strong>Ciudad: </strong> {{ $usuario->ciudad_ins }}</li>
            <li><strong>Correo: </strong> {{ $usuario->correo_ins }}</li>
            <li><strong>Comentarios: </strong> {{ $comentarios }}</li>
        </ul>
	</body>
</html>