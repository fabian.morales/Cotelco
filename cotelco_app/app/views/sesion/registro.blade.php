@extends('master')

@section('content')
<div class="row">
    <div class="small-10 columns small-centered">
        <h3 class="text-center">Formulario inscripci&oacute;n</h3>
        <form class="formulario" id="form_registro" name="form_registro" action="{{ url('/sesion/guardarRegistro') }}" method="post">
            <input type="hidden" id="email" name="email" value="" />
            <input type="hidden" id="admin" name="admin" value="" />
            <input type="hidden" id="activo" name="activo" value="" />
            <input type="hidden" id="url_ciudades" name="url_ciudades" value="{{ url('/sesion/obtenerCiudades/') }}" />
            <ul class="tabs" data-tab>
                <li class="tab-title active"><a id="tabDatosHotel" href="#info_hotel">Datos del Hotel</a></li>
                <li class="tab-title"><a id="tabDatosRepr" href="#info_repr">Datos del representante legal</a></li>
                <li class="tab-title"><a id="tabDatosIns" href="#info_ins">Datos de quien realiza la autoevaluaci&oacute;n</a></li>
            </ul>
            <div class="tabs-content">
                <div class="content active" id="info_hotel">
                    <div class="row">
                        <h4 class="text-center">Datos del hotel</h4>
                        <div class="small-12 columns">Nombre del hotel</div>
                        <div class="small-12 columns">
                            <input type="text" name="nombre" id="nombre" placeholder="" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">NIT</div>
                        <div class="small-12 columns">
                            <input type="text" name="login" id="login" placeholder="" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">RNT</div>
                        <div class="small-12 columns">
                            <input type="text" name="rnt" id="rnt" placeholder="" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            Clave
                        </div>
                        <div class="small-12 columns">
                            <input type="password" name="password" id="password" placeholder="" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12 columns">Departamento</div>
                                <div class="small-12 columns">
                                    <select id="depto" name="depto" required data-select="#ciudad">
                                        <option></option>
                                        @foreach($deptos as $d)
                                        <option value="{{ $d->id }}">{{ $d->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12 columns">Ciudad</div>
                                <div class="small-12 columns">
                                    <select id="ciudad" name="ciudad" required>
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12">Direcci&oacute;n</div>
                                <div class="small-12"><input type="text" id="direccion" name="direccion" required /></div>
                            </div>
                        </div>
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12">Tel&eacute;fono</div>
                                <div class="small-12"><input type="text" id="telefono" name="telefono" required /></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <input type="button" rel="navIns" data-target="#tabDatosRepr" value="" class="boton_sig right" />
                        </div>
                    </div>
                </div>
                
                <div class="content" id="info_repr">
                    <div class="row">
                        <h4 class="text-center">Datos del representante legal</h4>
                        <div class="small-12 columns">Nombre</div>
                        <div class="small-12 columns">
                            <input type="text" name="nombre_repr" id="nombre_repr" placeholder="" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">N&uacute;mero de c&eacute;dula</div>
                        <div class="small-12 columns">
                            <input type="text" name="cedula_repr" id="cedula_repr" placeholder="" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12 columns">Departamento</div>
                                <div class="small-12 columns">
                                    <select id="depto_repr" name="depto_repr" required data-select="#ciudad_repr">
                                        <option></option>
                                        @foreach($deptos as $d)
                                        <option value="{{ $d->id }}">{{ $d->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12 columns">Ciudad</div>
                                <div class="small-12 columns">
                                    <select id="ciudad_repr" name="ciudad_repr" required>
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12">Direcci&oacute;n</div>
                                <div class="small-12"><input type="text" id="direccion_repr" name="direccion_repr" required /></div>
                            </div>
                        </div>
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12">Tel&eacute;fono</div>
                                <div class="small-12"><input type="text" id="telefono_repr" name="telefono_repr" required /></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">Celular</div>
                        <div class="small-12 columns">
                            <input type="text" name="celular_repr" id="celular_repr" placeholder="" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">Correo</div>
                        <div class="small-12 columns">
                            <input type="email" name="correo_repr" id="correo_repr" placeholder="" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <input type="button" rel="navIns" data-target="#tabDatosHotel" value="" class="boton_ant left" />
                            <input type="button" rel="navIns" data-target="#tabDatosIns" value="" class="boton_sig right" />
                        </div>
                    </div>
                </div>
                
                <div class="content" id="info_ins">
                    <div class="row">
                        <h4 class="text-center">Datos de quien hace la autoevaluaci&oacute;n</h4>
                        <div class="small-12 columns">Nombre</div>
                        <div class="small-12 columns">
                            <input type="text" name="nombre_ins" id="nombre_ins" placeholder="" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">N&uacute;mero de c&eacute;dula</div>
                        <div class="small-12 columns">
                            <input type="text" name="cedula_ins" id="cedula_ins" placeholder="" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12 columns">Departamento</div>
                                <div class="small-12 columns">
                                    <select id="depto_ins" name="depto_ins" required data-select="#ciudad_ins">
                                        <option></option>
                                        @foreach($deptos as $d)
                                        <option value="{{ $d->id }}">{{ $d->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12 columns">Ciudad</div>
                                <div class="small-12 columns">
                                    <select id="ciudad_ins" name="ciudad_ins" required>
                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12">Direcci&oacute;n</div>
                                <div class="small-12"><input type="text" id="direccion_ins" name="direccion_ins" required /></div>
                            </div>
                        </div>
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12">Tel&eacute;fono</div>
                                <div class="small-12"><input type="text" id="telefono_ins" name="telefono_ins" required /></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">Celular</div>
                        <div class="small-12 columns">
                            <input type="text" name="celular_ins" id="celular_ins" placeholder="" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12 columns">Correo</div>
                                <div class="small-12 columns">
                                    <input type="email" name="correo_ins" id="correo_ins" placeholder="" required />
                                </div>
                            </div>
                        </div>
                        <div class="small-6 columns">
                            <div class="row">
                                <div class="small-12 columns">Cargo</div>
                                <div class="small-12 columns">
                                    <input type="text" name="cargo_ins" id="cargo_ins" placeholder="" required />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <input type="button" rel="navIns" data-target="#tabDatosRepr" value="" class="boton_ant left" />
                            <input type="submit" value="" class="boton_sig right" />
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop