<div class="row">
    <div class="large-12 columns">
        <h2>Permisos usuario</h2>
        <form id="form_permisos" name="form_permisos" action="{{ url('/usuario/guardarPermisos') }}" method="post">
            <input type="hidden" id="id_usuario" name="id_usuario" value="{{ $usuario->id }}" />
            @foreach ($controladores as $c)
            <div class="row">
                <div class="medium-10 small-7 columns">
                    <label for="c_{{ $c->id }}">{{ $c->nombre }}</label>
                </div>
                <div class="medium-2 small-5 columns">                    
                    <input type="checkbox" name="controlador[]" id="c_{{ $c->id }}" value="{{ $c->id }}" @if(sizeof($c->usuarios->first())) checked @endif/>
                </div>
            </div>
            @endforeach
            <div class="row">
                <div class="small-12 columns">
                    <input type="submit" value="Guardar" class="button default" />                    
                </div>
            </div>              
        </form>
    </div>
</div>