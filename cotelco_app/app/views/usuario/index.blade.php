@extends('master')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3>Lista de usuarios</h3>
    </div>
</div>
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('usuario/crear') }}" class="button" data-featherlight>Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
@include('usuario.lista', array("usuarios" => $usuarios))
@stop