<div class="row">
    <div class="small-12 columns">
        <table>
            <thead>
                <tr>
                    <th>Consecutivo</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Editar</th>                    
                </tr>
            </thead>
            <tbody>
                @foreach($usuarios as $u)
                <tr>
                    <td>{{ $u->id }}</td>
                    <td>{{ $u->nombre }}</td>
                    <td>{{ $u->email }}</td>
                    <td><a href="{{ url('/usuario/editar/'.$u->id) }}" data-featherlight><i class="fi-pencil"></i></a></td>                    
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="small-12 columns text-center">
        {{ $usuarios->links() }}
    </div>
</div>