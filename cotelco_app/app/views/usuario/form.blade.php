<div class="row">
    <div class="large-12 columns">
        <h2>Datos del usuario</h2>
        <form id="form_usuario" name="form_usuario" action="{{ url('usuario/guardar') }}" method="post">
            <input type="hidden" id="id" name="id" value="{{ $usuario->id }}" />
            <div class="row">
                <div class="medium-4 small-12 columns">
                    <label for="nombre">Nombre</label>
                </div>
                <div class="medium-8 small-12 columns">
                    <input type="text" name="nombre" id="nombre" value="{{ $usuario->nombre }}" />
                </div>
            </div>
            <div class="row">
                <div class="medium-4 small-12 columns">
                    <label for="login">Login</label>
                </div>
                <div class="medium-8 small-12 columns">
                    <input type="text" name="login" id="login" value="{{ $usuario->login }}" />
                </div>
            </div>
            <div class="row">
                <div class="medium-4 small-12 columns">
                    <label for="password">Clave</label>
                </div>
                <div class="medium-8 small-12 columns">
                    <input type="password" name="password" id="password" value="" />
                </div>
            </div>
            <div class="row">
                <div class="medium-4 small-12 columns">
                    <label for="email">Correo electr&oacute;nico</label>
                </div>
                <div class="medium-8 small-12 columns">
                    <input type="text" name="email" id="email" value="{{ $usuario->email }}" />
                </div>
            </div>
            <div class="row">
                <div class="medium-4 small-12 columns">
                    <label>Administrador</label>
                </div>
                <div class="medium-8 small-12 columns columns">
                    <input type="radio" name="admin" id="admin_si" value="Y" @if($usuario->admin == "Y") checked @endif /><label for="admin_si">Si</label>
                    <input type="radio" name="admin" id="admin_no" value="N" @if($usuario->admin == "N") checked @endif /><label for="admin_no">No</label>
                </div>
            </div>            
            <div class="row">
                <div class="medium-4 small-12 columns">
                    <label>Activo</label>
                </div>
                <div class="medium-8 small-12 columns columns">
                    <input type="radio" name="activo" id="activo_si" value="Y" @if($usuario->activo == "Y") checked @endif /><label for="activo_si">Si</label>
                    <input type="radio" name="activo" id="activo_no" value="N" @if($usuario->activo == "N") checked @endif /><label for="activo_no">No</label>
                </div>
            </div>   
            <div class="row">
                <div class="small-12 columns">
                    <input type="submit" value="Guardar" class="button default" />                    
                </div>
            </div>              
        </form>
    </div>
</div>