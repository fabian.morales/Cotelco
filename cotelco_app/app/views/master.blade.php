<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Cotelco</title>
    <link href="{{ asset('foundation/css/foundation.css') }}" rel="stylesheet" />
    <link href="{{ asset('foundation/fonts/foundation-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/featherlight/featherlight.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/jquery-ui/jquery-ui.theme.css') }}" rel="stylesheet" />    
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet" />        
    <link href="{{ asset('favicon.ico') }}" rel="shortcut icon" /-->
    @section ('css_header')
    @show
    
    <script src="{{ asset('js/jquery-1.11.2.min.js') }}"></script>
    <script src="{{ asset('foundation/js/foundation.min.js') }}"></script>    
    <script src="{{ asset('foundation/js/vendor/modernizr.js') }}"></script>    
    <script src="{{ asset('js/featherlight/featherlight.js') }}"></script>
    <script src="{{ asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('js/jquery.validate/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/jquery.validate/additional-methods.js') }}"></script>
    <script src="{{ asset('js/jquery.validate/localization/messages_es.js') }}"></script>
    <script src="{{ asset('js/jquery.validate/localization/methods_es_CL.js') }}"></script>
    <script src="{{ asset('js/site.js') }}" language="javascript"></script>
    <!--script src="{{ asset('js/jquery.validate/jquery.validate.js') }}"></script>
    
    <!--script src="{{ asset('js/modernizr-2.6.2.min.js') }}"></script>
    <script src="{{ asset('js/gumby.min.js') }}"></script-->    
    @section ('js_header')
    @show
</head>
<body>
    <div id="wrapper">
        @if (Auth::check())
        @if (Auth::user()->admin == "Y")
        <nav class="top-bar" data-topbar role="navigation">
            <ul class="title-area">
                <li class="name">
                    <h1><a href="index.php">Inicio</a></h1>
                </li>            
                <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>
            <section class="top-bar-section">
                <!-- Left Nav Section -->            
                <ul class="left">
                  <li><a href="{{ url('/usuario/') }}">Usuarios</a></li>
                  <li><a href="{{ url('/opcion/') }}">Opciones de preguntas</a></li>
                  <li><a href="{{ url('/grupo/') }}">Grupos y preguntas</a></li>
                  <!--li><a href="{{ url('/evaluacion/') }}">Evaluaci&oacute;n</a></li-->
                  <!--li><a href="{{ url('/familia/') }}">Familias de productos</a></li>
                  <li><a href="{{ url('/estado/') }}">Estados de productos</a></li>
                  <li><a href="{{ url('/pedido/') }}">Pedidos</a></li>
                  <li><a href="{{ url('/datalog/') }}">Hist&oacute;rico de cambios</a></li>
                  <li><a href="{{ url('/controlador/') }}">Secciones</a></li-->              
                </ul>

                <!-- Right Nav Section -->
                <ul class="right">
                  <li><a href="{{ url('/sesion/logout') }}">Cerrar sesi&oacute;n</a></li>
                </ul>
          </section>
        </nav>
        @endif
        @endif
        <div id="div_loading"></div>
        <header>
            <div class="row fullWidth collapse">
                <div class="small-9 columns text-center">
                    <img class="logo" src="{{ asset('img/logo.png') }}" />
                </div>
                <div class="small-3 columns">
                    <div id="menu_superior">
                        <div class="row">
                            <div class="small-2 columns">&nbsp;</div>
                            <div class="small-3 columns"><a href="{{ url('/') }}"><img src="{{ asset('img/icono_home.png') }}" />Inicio</a></div>
                            <div class="small-3 columns"><a href="{{ url('/sesion/ayuda') }}"><img src="{{ asset('img/icono_ayuda.png') }}" />Ayuda</a></div>
                            <div class="small-3 columns end"><a href="#" data-reveal-id="iframe_tutorial"><img src="{{ asset('img/icono_tutorial.png') }}" />Tutorial</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>        
        <div class="row msg">
            <div class="small-12 columns">
                <div class="mensajes">
                    @if (Session::has('mensajeError'))            
                    <div data-alert class="alert-box alert radius">
                        {{ Session::get('mensajeError') }}
                        <a href="#" class="close">&times;</a>
                    </div>            
                    @endif
                    @if (Session::has('mensaje'))
                    <div data-alert class="alert-box success radius">
                        {{ Session::get('mensaje') }}
                        <a href="#" class="close">&times;</a>
                    </div>
                    @endif 
                    @if (Session::has('mensajeExt'))            
                        {{ Session::get('mensajeExt') }}            
                    @endif
                </div>
            </div>
        </div>  
        <div class="separador_header">&nbsp;</div> 
        @yield('content')
        <footer>
            All rights reserved COTELCO 2015 &copy;
        </footer>
        <div class="reveal-modal large" id="iframe_tutorial" data-reveal>
            <div class="flex-video widescreen youtube">
                <iframe width="1280" height="720" src="https://www.youtube.com/embed/zJZ-AlDuRcY" frameborder="0" allowfullscreen></iframe>
            </div>
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        </div>
    </div>        
    </div>
</body>
</html>
