@extends('master')

@section('content')
<div class="row fullWidth collapse contenido_home">
    <div class="small-7 columns">
        <div class="row">
            <div class="small-12 columns">
                <h1 class="text-center">Herramienta de Autoevaluaci&oacute;n</h1>
                <h1 class="text-center" style="border-bottom: 2px solid #000">NTS TS 002</h1>
                <hr />
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <div class="seccion-cont-home">
                    <p>Ustedes hacen parte del crecimiento y desarrollo de la zona donde prestan sus
                    servicios, por lo cual, la huella que dejan en estas poblaciones debe ser
                    <strong>ecol&oacute;gicamente sostenible, responsable y consciente,</strong>
                    para generar acciones que perduran en el tiempo.</p>
                    <p>Cada acci&oacute;n cuenta, y cada comunidad donde est&aacute;n establecidos
                    cuenta con ustedes. As&iacute; que desde ya <strong>empecemos a generar
                    resultados</strong> concretos que se conecten con la realidad de cada uno de
                    ustedes.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-10 columns small-centered medium-centered large-centered">
                <div class="row">
                    <div class="small-6 columns">
                        <a class="boton-home boton1" href="{{ url('/sesion/registro') }}"><span>Inscripci&oacute;n</span></a>
                    </div>
                    <div class="small-6 columns">
                        <a class="boton-home boton2" href="{{ url('/sesion/formLogin') }}"><span>Inicie <strong>la autoevaluaci&oacute;n</strong></span></a>
                    </div>
                </div>
                <div class="clearfix separador_verde">&nbsp;</div>
                <div class="row">
                    <div class="small-6 columns">
                        <a class="boton-home boton3" href="{{ asset('pdf/guia_implementacion.pdf') }}" target="_blank"><span>Descargue la <strong>Gu&iacute;a</strong></span></a>
                    </div>
                    <div class="small-6 columns">
                        <a class="boton-home boton4" href="{{ asset('pdf/NTS_TS_002.pdf') }}" target="_blank"><span>Descargue la <strong>Norma de Sostenibilidad</strong></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="grupos_home small-5 columns">
        @foreach($grupos as $g)
            <div class="row">
                <div class="small-7 columns text-center"><a href="{{ url('/evaluacion/preguntas/'.$g->preguntaNivelMap->pagina_inicio) }}"><img src="{{ asset('img/seccion_'.$g->nivel1.'/titulo.png') }}" /></a></div>
                <div class="small-5 columns"><a href="{{ url('/evaluacion/preguntas/'.$g->preguntaNivelMap->pagina_inicio) }}"><img src="{{ asset('img/seccion_'.$g->nivel1.'/logo.png') }}" /></a></div>
            </div>
        @endforeach
    </div>
</div>
@stop