<div class="row">
    <div class="large-12 columns">
        <h2>Datos de la opci&oacute;n</h2>
        <form id="form_opcion" name="form_opcion" action="{{ url('opcion/guardar') }}" method="post">
            <input type="hidden" id="id" name="id" value="{{ $opcion->id }}" />
            <div class="row">
                <div class="medium-4 small-12 columns">
                    <label for="nombre">Nombre</label>
                </div>
                <div class="medium-8 small-12 columns">
                    <input type="text" name="nombre" id="nombre" value="{{ $opcion->nombre }}" />
                </div>
            </div>
            <div class="row">
                <div class="medium-4 small-12 columns">
                    <label for="valor">Valor</label>
                </div>
                <div class="medium-8 small-12 columns">
                    <input type="text" name="valor" id="valor" value="{{ $opcion->valor }}" />
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <input type="submit" value="Guardar" class="button default" />                    
                </div>
            </div>              
        </form>
    </div>
</div>