@extends('master')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3>Lista de opciones de preguntas</h3>
    </div>
</div>
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('opcion/crear') }}" class="button" data-featherlight>Nueva <i class="fi-plus"></i></a>
    </div>
</div>
@include('opcion.lista', array("opciones" => $opciones))
@stop