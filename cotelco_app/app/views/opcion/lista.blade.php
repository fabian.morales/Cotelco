<div class="row">
    <div class="small-12 columns">
        <table>
            <thead>
                <tr>
                    <th>Consecutivo</th>
                    <th>Nombre</th>                    
                    <th>Editar</th>                    
                </tr>
            </thead>
            <tbody>
                @foreach($opciones as $o)
                <tr>
                    <td>{{ $o->id }}</td>
                    <td>{{ $o->nombre }}</td>                    
                    <td><a href="{{ url('/opcion/editar/'.$o->id) }}" data-featherlight><i class="fi-pencil"></i></a></td>                    
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="small-12 columns text-center">
        {{ $opciones->links() }}
    </div>
</div>


