<?php
class Mensaje extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sis_respuesta_mensaje';
    protected $fillable = array('id', 'id_pregunta', 'id_opcion', 'mensaje');
    
    public function opcion(){
        return $this->belongsTo('Opcion', 'id_opcion');
    }
    
    public function pregunta(){
        return $this->belongsTo('Pregunta', 'id_pregunta');
    }
}