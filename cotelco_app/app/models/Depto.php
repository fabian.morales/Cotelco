<?php

class Depto extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sis_depto';
    
    public function ciudades(){
        return $this->hasMany('Ciudad', 'id_depto');
    }
}