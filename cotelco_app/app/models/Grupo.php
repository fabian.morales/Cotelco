<?php
class Grupo extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sis_grupo';
    protected $fillable = array('id', 'nivel1', 'nivel2', 'nivel3', 'nivel4', 'nivel5', 'nombre', 'descripcion', 'id_grupo', 'orden');
    
    public function subgrupos(){
        return $this->hasMany('Grupo', 'id_grupo');
    }
    
    public function preguntas(){
        return $this->hasMany('Pregunta', 'id_grupo');
    }
    
    public function preguntasGrupo(){
        return $this->hasManyThrough('Pregunta', 'Grupo', 'id_grupo', 'id_grupo');
    }
    
    public function preguntaMap(){
        return $this->hasMany('PreguntaMap', 'id_grupo');
    }
    
    public function preguntaNivelMap(){
        return $this->hasOne('PreguntaNivelMap', 'nivel1', 'nivel1');
    }
    
    public function mostrarNivel(){
        $ret = $this->nivel1
               .(!empty($this->nivel2) ? '.'.$this->nivel2 : '')
               .(!empty($this->nivel3) ? '.'.$this->nivel3 : '')
               .(!empty($this->nivel4) ? '.'.$this->nivel4 : '')
               .(!empty($this->nivel5) ? '.'.$this->nivel5 : '');
        return trim($ret);
    }
    
    public function obtenerDatosGrafico($idUsuario){
        return Grupo::from('sis_grupo')
               ->join('sis_pregunta', 'sis_pregunta.id_grupo', '=', 'sis_grupo.id') 
               ->leftJoin('sis_respuesta', function($join) use ($idUsuario){
                   $join->on('sis_pregunta.id', '=', 'sis_respuesta.id_pregunta')
                        ->on('sis_respuesta.id_usuario', '=', DB::raw($idUsuario));
               })
               ->leftJoin('sis_opcion_pregunta', function($join){
                   $join->on('sis_respuesta.id_opcion', '=', 'sis_opcion_pregunta.id')
                        ->on('sis_opcion_pregunta.valor', '>', DB::raw(0));
               })
               ->select('sis_grupo.nivel1', 
                        DB::raw("concat(sis_grupo.nivel1, coalesce(concat('.', sis_grupo.nivel2), ''),  coalesce(concat('.', sis_grupo.nivel3), ''), coalesce(concat('.', sis_grupo.nivel4), ''), coalesce(concat('.', sis_grupo.nivel5), '')) numeral"),
                        DB::raw("count(1) total"),
                        DB::raw("count(case when sis_respuesta.id_opcion = 1 or sis_respuesta.id_opcion = 3 then 1 else null end) cumple"),
                        DB::raw("(count(case when sis_respuesta.id_opcion = 1 or sis_respuesta.id_opcion = 3 then 1 else null end) / count(1)) * 100 cumplimiento"))
                ->groupBy('sis_pregunta.id_grupo')
                ->groupBy('sis_grupo.nivel1')
                ->where("sis_grupo.nivel1", $this->nivel1)
                ->get();
    }
}