<?php
class PreguntaNivelMap extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sis_pregunta_nivel_map';
    
    public function grupo(){
        return $this->belongsTo('PreguntaNivelMap', 'nivel1', 'nivel1');
    }
}