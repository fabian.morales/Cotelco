<?php
class Respuesta extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sis_respuesta';
    
    public function opcion(){
        return $this->belongsTo("Opcion", "id_opcion");
    }
}