<?php
class Pregunta extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sis_pregunta';
    protected $fillable = array('id', 'enunciado', 'id_grupo', 'orden');
    
    public function mensajes(){
        return $this->hasMany('Mensaje', 'id_pregunta');
    }
    
    public function respuestas(){
        return $this->hasMany('Respuesta', 'id_pregunta');
    }
    
    public function recortarEnunciado(){    
        //return $texto;
        $aux = wordwrap(trim(strip_tags($this->enunciado)), 1500);        
        $partes = explode("\n", $aux);
        
        if (sizeof($partes)){                        
            foreach ($partes as $p){
                if (strlen($p) > 1000){
                    return $p.'...';
                }
            }
            
            return $partes[0];
        }
        
        return $aux;
    }
}