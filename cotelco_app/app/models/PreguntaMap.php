<?php
class PreguntaMap extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sis_pregunta_map';    
    
    public function pregunta(){
        return $this->hasOne('Pregunta', 'id', 'id_pregunta');
    }
    
    public function grupo(){
        return $this->hasOne('Grupo', 'id', 'id_grupo');
    }
    
    public static function generarLink($pag, $tipo="sig"){
        $ret = "";
        $minNivel = PreguntaNivelMap::min("nivel1");
        $maxNivel = PreguntaNivelMap::max("nivel1");
        
        $map = PreguntaNivelMap::with("grupo")
                                ->where("pagina_inicio", "<=", $pag)
                                ->where("pagina_fin", ">=", $pag)
                                ->first();

        if ($tipo == "sig"){
            if ($pag == $map->pagina_fin){
                $ret = url("/evaluacion/transicion/".$map->nivel1);
            }
            else{
                $ret = url("/evaluacion/preguntas/".($pag + 1));
            }
        }
        else{
            if ($pag == $map->pagina_inicio && $map->nivel1 > $minNivel){
                $ret = url("/evaluacion/transicion/".($map->nivel1 - 1));
            }
            else if ($pag > $map->pagina_inicio){
                $ret = url("/evaluacion/preguntas/".($pag - 1));
            }
        }
        
        return $ret;
    }
}