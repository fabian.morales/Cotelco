<?php
class Opcion extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sis_opcion_pregunta';
    protected $fillable = array('id', 'nombre', 'valor');
    
    public function mensajes(){
        return $this->hasMany('Mensaje', 'id_opcion');
    }
}