<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@mostrarIndex');
Route::get('/sesion/registro', 'SesionController@registrarse');
Route::get('/sesion/obtenerCiudades/{idDepto}', 'SesionController@obtenerCiudades');
Route::post('/sesion/guardarRegistro', 'SesionController@guardarRegistro');
Route::get('/sesion/formLogin', 'SesionController@mostrarIndex');
Route::post('/sesion/login', 'SesionController@hacerLogin');
Route::get('/sesion/logout', 'SesionController@hacerLogout');
Route::get('/sesion/ayuda', 'SesionController@mostrarAyuda');
Route::post('/sesion/ayuda/enviar', 'SesionController@enviarAyuda');

Route::get('/usuario/', 'UsuarioController@mostrarIndex');
Route::get('/usuario/crear', 'UsuarioController@crearUsuario');
Route::get('/usuario/editar/{id}', 'UsuarioController@editarUsuario');
Route::post('/usuario/guardar', 'UsuarioController@guardarUsuario');
Route::get('/usuario/permisos/{id}', 'UsuarioController@mostrarFormPermisos');

Route::get('/grupo/', 'GrupoController@mostrarIndex');
Route::get('/grupo/crear', 'GrupoController@crearGrupo');
Route::get('/grupo/editar/{id}', 'GrupoController@editarGrupo');
Route::post('/grupo/guardar', 'GrupoController@guardarGrupo');
Route::get('/grupo/reconstruir', 'GrupoController@reconstruirGruposPreguntas');

Route::get('/pregunta/', 'GrupoController@mostrarIndex');
Route::get('/pregunta/crear/{id}', 'GrupoController@crearPregunta');
Route::get('/pregunta/editar/{id}', 'GrupoController@editarPregunta');
Route::post('/pregunta/guardar', 'GrupoController@guardarPregunta');

Route::get('/opcion/', 'OpcionController@mostrarIndex');
Route::get('/opcion/crear', 'OpcionController@crearOpcion');
Route::get('/opcion/editar/{id}', 'OpcionController@editarOpcion');
Route::post('/opcion/guardar', 'OpcionController@guardarOpcion');

Route::get('/evaluacion/', 'EvaluacionController@mostrarIndex');
Route::get('/evaluacion/preguntas/{pag}', 'EvaluacionController@mostrarPreguntas');
Route::get('/evaluacion/transicion/{nivel}', 'EvaluacionController@mostrarTransicion');
Route::get('/evaluacion/grafico/{nivel}', 'EvaluacionController@mostrarGrafico');
Route::get('/evaluacion/graficoFinal', 'EvaluacionController@mostrarGraficoFinal');
Route::post('/evaluacion/guardar', 'EvaluacionController@guardarRespuestas');
Route::get('/evaluacion/tareas/{nivel}', 'EvaluacionController@generarListaTareasIndividual');
Route::get('/evaluacion/tareasFinal', 'EvaluacionController@generarListaTareasFinal');
Route::get('/evaluacion/reporteParcial/{nivel}', 'EvaluacionController@generarReporteParcial');
Route::get('/evaluacion/reporte', 'EvaluacionController@generarReporteFinal');

Route::get('/crearAdmin', function() { 
    $usuario = User::find(1);
    //$usuario = new User();
    //$usuario->nombre = "Admin";
    //$usuario->login = "admin";
    $usuario->password = Hash::make("Ev4nerv");
    $usuario->admin = "Y";
    $usuario->save();
});