<?php

class EvaluacionController extends BaseController {
   public function mostrarIndex(){
        return $this->mostrarPreguntas(1);
    }
    
    public function mostrarPreguntas($pag){
        if (!$pag){
            $pag = 1;
        }
        
        $usuario = Auth::user();
        $items = PreguntaMap::with(array("pregunta.respuestas" => function($query) use ($usuario) {
            $query->where("id_usuario", $usuario->id);
        }, "grupo"))->where("pagina", $pag)->orderBy("cnt", "asc")->get();
        
        if (!sizeof($items)){
            return Redirect::action('HomeController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el grupo");
        }       
        
        $opciones = Opcion::all();
        $grupos = Grupo::with("preguntaNivelMap")->whereRaw("id_grupo is null")->orderBy("id", "asc")->get();
        return View::make('evaluacion.preguntas', array("items" => $items, "opciones" => $opciones, "grupos" => $grupos));
    }
        
    public function guardarRespuestas(){
        $respuestasOpc = Input::get("opc");
        $respuestasTxt = Input::get("tarea");
        if (sizeof($respuestasOpc)){
            $usuario = Auth::user();
            
            foreach ($respuestasTxt as $idPregunta => $t){
                $resp = Respuesta::where("id_usuario", $usuario->id)->where("id_pregunta", $idPregunta)->first();
                if (!sizeof($resp)){
                    $resp = new Respuesta();
                    $resp->id_usuario = $usuario->id;
                    $resp->id_pregunta = $idPregunta;                
                }
                
                $resp->observaciones = $t;
                $resp->id_opcion = array_key_exists($idPregunta, $respuestasOpc) ? $respuestasOpc[$idPregunta] : null;
                $resp->save();
            }
        }
        
        return "ok";
    }
    
    public function mostrarTransicion($nivel){
        $minNivel = PreguntaNivelMap::min("nivel1");
        $maxNivel = PreguntaNivelMap::max("nivel1");
        
        $grupo = Grupo::where("nivel1", $nivel)->first();
        $grupoSig = null;
        if ($nivel < $maxNivel){
            $grupoSig = Grupo::with("preguntaNivelMap")->where("nivel1", $nivel + 1)->first();
        }
        
        $grupos = Grupo::with("preguntaNivelMap")->whereRaw("id_grupo is null")->orderBy("id", "asc")->get();
        return View::make('evaluacion.transicion', array("grupo" => $grupo, "grupoSig" => $grupoSig, "grupos" => $grupos));
    }
    
    public function mostrarGrafico($nivel){        
        $usuario = Auth::user();
        $sql = "SELECT b.id_grupo, d.nivel1, ".
               "concat(d.nivel1, coalesce(concat('.', d.nivel2), ''),  coalesce(concat('.', d.nivel3), ''), coalesce(concat('.', d.nivel4), ''), coalesce(concat('.', d.nivel5), '')) numeral, ".
               "count(1) total, ".
               "count(case when a.id_opcion = 1 or a.id_opcion = 3 then 1 else null end) cumple, ".
               "(count(case when a.id_opcion = 1 or a.id_opcion = 3 then 1 else null end) / count(1)) * 100 cumplimiento ".
               "FROM sis_grupo d ".
               "inner join sis_pregunta b on (b.id_grupo = d.id) ".
               "left join sis_respuesta a on (a.id_pregunta = b.id and a.id_usuario = :idUsuario) ".
               "left join sis_opcion_pregunta c on (a.id_opcion = c.id and c.valor > 0) ".
               "where d.nivel1 = :nivel ".             
               "group by b.id_grupo, d.nivel1";

        $resultados = DB::select(DB::raw($sql), array('nivel' => $nivel, 'idUsuario' => $usuario->id));
        $grupo = Grupo::where("nivel1", $nivel)->first();
        $gruposNiv = Grupo::where("nivel1", $nivel)->get();
        $grupos = Grupo::with("preguntaNivelMap")->whereRaw("id_grupo is null")->orderBy("id", "asc")->get();        
        return View::make('evaluacion.grafico', array("grupo" => $grupo, "resultados" => $resultados, "grupos" => $grupos, "usuario" => $usuario, "gruposNiv" => $gruposNiv));
    }
    
    public function mostrarGraficoFinal(){        
        $usuario = Auth::user();
        $grupos = Grupo::whereRaw("id_grupo is null")->get();
        $gruposMap = Grupo::with("preguntaNivelMap")->whereRaw("id_grupo is null")->orderBy("id", "asc")->get();        
        return View::make('evaluacion.grafico_final', array("grupos" => $grupos, "gruposMap" => $gruposMap, "usuario" => $usuario));
    }
    
    public function generarListaTareasIndividual($nivel){
        $usuario = Auth::user();
        $grupos = Grupo::with(array("preguntas.respuestas" => function($query) use ($usuario) {
            $query->where("id_usuario", $usuario->id);
        }))->where("nivel1", $nivel)->get();
        //return View::make('evaluacion.lista_tareas_final', array("grupos" => $grupos, "usuario" => $usuario));
        $html = View::make('evaluacion.lista_tareas_final', array("grupos" => $grupos, "usuario" => $usuario))->render();
        
        $pdf = new myPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);        
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('FrontierSoft');
        $pdf->SetTitle('Autoevaluacion');
        $pdf->SetSubject('Lista Tareas Parcial');
        $pdf->SetKeywords('cotelco');
        
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
        
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetFont('helvetica', '', 11);

        $pdf->SetMargins(PDF_MARGIN_LEFT, 45, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, 16);
        $pdf->setImageScale(1.6);
        $pdf->setJPEGQuality(100);
        
        $pdf->AddPage('P', 'Letter');
        
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output();
    }
    
    public function generarListaTareasFinal(){
        $usuario = Auth::user();
        $grupos = Grupo::with(array("preguntas.respuestas" => function($query) use ($usuario) {
            $query->where("id_usuario", $usuario->id);
        }))->get();
        $html = View::make('evaluacion.lista_tareas_final', array("grupos" => $grupos, "usuario" => $usuario))->render();
        
        $pdf = new myPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);        
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('FrontierSoft');
        $pdf->SetTitle('Autoevaluacion');
        $pdf->SetSubject('Lista Tareas Final');
        $pdf->SetKeywords('cotelco');
        
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
        
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetFont('helvetica', '', 11);

        $pdf->SetMargins(PDF_MARGIN_LEFT, 45, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, 16);
        $pdf->setImageScale(1.6);
        $pdf->setJPEGQuality(100);

        $pdf->AddPage('P', 'Letter');
        
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output();
    }
    
    public function generarReporteParcial($nivel){
        $usuario = Auth::user();
        $grupos = Grupo::with(array("preguntas.respuestas" => function($query) use ($usuario) {
            $query->where("id_usuario", $usuario->id);
        }, "preguntas.respuestas.opcion"))->where("nivel1", $nivel)->get();
        $html = View::make('evaluacion.reporte', array("grupos" => $grupos, "usuario" => $usuario))->render();
        
        $pdf = new myPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);        
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('FrontierSoft');
        $pdf->SetTitle('Autoevaluacion');
        $pdf->SetSubject('Reporte parcial');
        $pdf->SetKeywords('cotelco');
        
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
        
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetFont('helvetica', '', 11);

        $pdf->SetMargins(PDF_MARGIN_LEFT, 45, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, 16);
        $pdf->setImageScale(1.6);
        $pdf->setJPEGQuality(100);

        $pdf->AddPage('P', 'Letter');
        
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output();
    }
    
    public function generarReporteFinal(){
        $usuario = Auth::user();
        $grupos = $grupos = Grupo::with(array("preguntas.respuestas" => function($query) use ($usuario) {
            $query->where("id_usuario", $usuario->id);
        }, "preguntas.respuestas.opcion"))->get();
        $html = View::make('evaluacion.reporte', array("grupos" => $grupos, "usuario" => $usuario))->render();
        
        $pdf = new myPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);        
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('FrontierSoft');
        $pdf->SetTitle('Autoevaluacion');
        $pdf->SetSubject('Reporte final');
        $pdf->SetKeywords('cotelco');
        
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
        
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetFont('helvetica', '', 11);

        $pdf->SetMargins(PDF_MARGIN_LEFT, 45, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, 16);
        $pdf->setImageScale(1.6);
        $pdf->setJPEGQuality(100);

        $pdf->AddPage('P', 'Letter');
        
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output();
    }
}