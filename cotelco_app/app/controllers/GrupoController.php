<?php

class GrupoController extends AdminController {
   public function mostrarIndex(){  
        $grupos = Grupo::orderBy("nivel1", "asc")
                        ->orderBy("nivel2", "asc")
                        ->orderBy("nivel3", "asc")
                        ->orderBy("nivel4", "asc")
                        ->orderBy("nivel5", "asc")
                        ->paginate(20);
        return View::make('grupo.index', array("grupos" => $grupos));
    }
       
    public function mostrarFormGrupo($grupo){       
        if (!sizeof($grupo)){
            $grupo = new Grupo();
        }
        $grupos = Grupo::whereRaw("id_grupo is null")->get();
        return View::make("grupo.form", array("grupo" => $grupo, "grupos" => $grupos));
    }
    
    public function crearGrupo(){
        if (Auth::user()->admin != 'Y'){
            return Redirect::to("/")->with("mensajeError", "No tiene permitido el acceso a esta opci&oacute;n");
        }
        
        return $this->mostrarFormGrupo(new Grupo());
    }
    
    public function editarGrupo($id){
        if (Auth::user()->admin != 'Y'){
            return Redirect::to("/")->with("mensajeError", "No tiene permitido el acceso a esta opci&oacute;n");
        }
        
        $grupo = Grupo::find($id);
        if (!sizeof($grupo)){
            return Redirect::action('GrupoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el grupo");
        }
        
        return $this->mostrarFormGrupo($grupo);
    }
    
    public function guardarGrupo(){
        if (Auth::user()->admin != 'Y'){
            return Redirect::to("/")->with("mensajeError", "No tiene permitido el acceso a esta opci&oacute;n");
        }
        
        $id = Input::get("id");        
        
        $grupo = Grupo::find($id);
        if (!sizeof($grupo)){
            $grupo = new Grupo();
        }
        
        $grupo->fill(Input::all());
        
        $grupo->nombre = ucfirst(strtolower($grupo->nombre));
        
        if (!(int)$grupo->id_grupo){
            $grupo->id_grupo = null;
        }
        
        $grupo->nivel1 = (int)$grupo->nivel1 ? $grupo->nivel1 : null;
        $grupo->nivel2 = (int)$grupo->nivel2 ? $grupo->nivel2 : null;
        $grupo->nivel3 = (int)$grupo->nivel3 ? $grupo->nivel3 : null;
        $grupo->nivel4 = (int)$grupo->nivel4 ? $grupo->nivel4 : null;
        $grupo->nivel5 = (int)$grupo->nivel5 ? $grupo->nivel5 : null;
        
        if ($grupo->save()){
            return Redirect::action('GrupoController@editarGrupo', array("id" => $grupo->id))->with("mensaje", "Grupo guardado exitosamente");
        }
        else{
            return Redirect::action('GrupoController@mostrarIndex')->with("mensajeError", "No se pudo guardar el grupo");
        }
    }
    
    public function mostrarFormPregunta($pregunta){       
        if (!sizeof($pregunta)){
            $pregunta = new Pregunta();
        }
        
        $opciones = Opcion::with(array('mensajes' => function($query) use ($pregunta) {
            $query->where('id_pregunta', $pregunta->id);
        }))->get();
        
        return View::make("grupo.form_pregunta", array("pregunta" => $pregunta, "opciones" => $opciones));
    }
    
    public function crearPregunta($id){
        if (Auth::user()->admin != 'Y'){
            return Redirect::to("/")->with("mensajeError", "No tiene permitido el acceso a esta opci&oacute;n");
        }
        
        $pregunta = new Pregunta();
        $pregunta->id_grupo = $id;
        return $this->mostrarFormPregunta($pregunta);
    }
    
    public function editarPregunta($id){
        if (Auth::user()->admin != 'Y'){
            return Redirect::to("/")->with("mensajeError", "No tiene permitido el acceso a esta opci&oacute;n");
        }
        
        $pregunta = Pregunta::where("id", $id)->with("mensajes.opcion")->first();        
        
        if (!sizeof($pregunta)){
            return Redirect::action('GrupoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar la pregunta");
        }
        
        return $this->mostrarFormPregunta($pregunta);
    }
    
    public function guardarPregunta(){
        if (Auth::user()->admin != 'Y'){
            return Redirect::to("/")->with("mensajeError", "No tiene permitido el acceso a esta opci&oacute;n");
        }
        
        $id = Input::get("id");        
        
        $pregunta = Pregunta::find($id);
        if (!sizeof($pregunta)){
            $pregunta = new Pregunta();
        }
        
        $pregunta->fill(Input::all());        
        
        if ($pregunta->save()){
            $opciones = Input::get("opcion");
            $mensajes = array();
            foreach($opciones as $k => $o){
                $mensaje = Mensaje::where("id_pregunta", $pregunta->id)->where("id_opcion", $k)->first();
                if (!sizeof($mensaje)){
                    $mensaje = new Mensaje();
                    $mensaje->id_pregunta = $pregunta->id;
                    $mensaje->id_opcion = $k;
                }
                
                $mensaje->mensaje = $o;
                $mensajes[] = $mensaje;
            }
            
            $pregunta->mensajes()->saveMany($mensajes);
            
            return Redirect::action('GrupoController@editarGrupo', array("id" => $pregunta->id_grupo))->with("mensaje", "Pregunta guardada exitosamente");
        }
        else{
            return Redirect::action('GrupoController@editarGrupo', array("id" => $pregunta->id_grupo))->with("mensajeError", "No se pudo guardar la pregunta");
        }
    }
    
    public function reconstruirGruposPreguntas(){
        DB::unprepared("truncate sis_pregunta_map");
        DB::unprepared("truncate sis_pregunta_nivel_map");
        DB::unprepared("insert into sis_pregunta_map (nivel1, id_grupo, id_pregunta, mostrar_grupo, grupo_ant, peso, pagina, cnt, cambio, nivel_ant) ".
                       "select a.nivel1, a.id id_grupo, b.id id_pregunta, ".
                       "case when a.id <> @gAnt then 1 else 0 end mostrar_grupo, ".
                       "@gAnt := a.id grupo_ant, ".
                       "@peso := case when b.id is null then 0 else 1 end peso, ".
                       "@pagina := case when @cambio = 1 or (@nivelAnt > 0 and @nivelAnt <> a.id and @cnt > 0) then @pagina + 1 else @pagina end pagina, ".
                       "@cnt := case when @cnt >= 3 or @cambio = 1 or (@nivelAnt > 0 and @nivelAnt <> a.id and @cnt > 0) then @peso else @cnt + @peso end cnt, ".
                       "@cambio := case when @cnt >= 3 then 1 else 0 end cambio, ".
                       "@nivelAnt := a.id nivel_ant ".
                       "from sis_grupo a ".
                       "left join sis_pregunta b on (a.id = b.id_grupo) ".
                       "join (select @cnt := 0, @pagina := 1, @peso := 0, @cambio := 0, @gAnt := 0, @nivelAnt := 0 from dual) t ".
                       "order by nivel1, nivel2, nivel3, nivel4, nivel5;");
                       
        DB::unprepared("insert into sis_pregunta_nivel_map(nivel1, pagina_inicio, pagina_fin) ".
                       "select nivel1, min(pagina) grupo_inicio, max(pagina) grupo_fin ".
                       "from sis_pregunta_map ".
                       "group by nivel1");
                       
        return Redirect::action('GrupoController@mostrarIndex')->with("mensaje", "Se ha reconstruido la estructura de grupos de preguntas");
    }}