<?php

class SesionController extends BaseController {

    public function mostrarIndex(){
        if (Auth::check()){
            return Redirect::to("/evaluacion/");
        }
        else{
            return View::make('sesion.login');
        }
    }
    
	public function obtenerCiudades($idDepto){
        $ciudades = Ciudad::where("id_depto", "=", $idDepto)->orderBy("id", "Asc")->get();
		return Response::json($ciudades);
    }
    
    public function registrarse(){
        $deptos = Depto::orderBy("nombre", "asc")->get();
        return View::make('sesion.registro', array("deptos" => $deptos));
    }
    
    public function guardarRegistro(){
        $usuarioAnt = User::where("login", Input::get("login"))->get();
        if (sizeof($usuarioAnt)){
            return Redirect::to("/sesion/registro/")->with("mensajeError", "Ya hay un hotel registro con el NIT ingresado");            
        }
        $usuario = new User();
        $usuario->fill(Input::all());
        $usuario->email = "";
        $usuario->admin = "N";
        $usuario->activo = "Y";
        
        $clave = Input::get("password");
        $usuario->password = Hash::make($clave);
        
        if ($usuario->save()){
            return Redirect::to("/sesion/formLogin")->with("mensaje", "Se ha registrado correctamente. Ahora puede iniciar sesión");
        }
        else{
            return Redirect::to("/sesion/registro")->with("mensajeError", "No se pudo realizar el registro");
        }
    }
       
    public function hacerLogin(){
        $login = Input::get("login");
        $clave = Input::get("password");
        if (Auth::attempt(array('login' => $login, 'password' => $clave))){        
            return Redirect::to("/evaluacion/");
        }
        else{
            return Redirect::to("/sesion/formLogin")->with("mensajeError", "Correo o clave incorrecta");
        }
    }
    
    public function hacerLogout(){
        Auth::logout();
        return Redirect::to("/sesion/formLogin");
    }
    
    public function mostrarAyuda(){
        if (!Auth::check()){
            return Redirect::to("/sesion/formLogin")->with("mensajeError", "Necesita estar logueado para ingresar a esta secci&oacute;n");
        }
        
        Session::put('redir_ayuda', URL::previous());
        return View::make("sesion.ayuda", array("usuario" => Auth::user()));
    }
    
    public function enviarAyuda(){
        if (!Auth::check()){
            return Redirect::to("/sesion/formLogin")->with("mensajeError", "Necesita estar logueado para ingresar a esta secci&oacute;n");
        }
        
        $comentarios = Input::get('comentarios');
        $usuario = Auth::user();
        Mail::send('sesion.email_ayuda', array("usuario" => $usuario, "comentarios" => $comentarios), function($message) use ($usuario){
            $message->to("ebernal@cotelco.org", "Cotelco")->subject('Solicitud de ayuda - '.$usuario->nombre);
            //$message->to("desarrollo@encubo.ws", "Cotelco")->subject('Solicitud de ayuda - '.$usuario->nombre);
        });
                
        $redir = Session::get('redir_ayuda');
        if (!empty($redir)){
            return Redirect::to($redir)->with('mensaje','Se ha enviado su solicitud de ayuda');
        }
        else{
            return Redirect::back()->with('mensaje','Se ha enviado su solicitud de ayuda');    
        }
    }
}