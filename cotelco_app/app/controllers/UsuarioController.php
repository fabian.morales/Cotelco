<?php

class UsuarioController extends AdminController {

    public function mostrarIndex(){       
        $usuarios = User::paginate(20);
        return View::make('usuario.index', array("usuarios" => $usuarios));
    }
    
    public function mostrarFormUsuario($usuario){       
        if (!sizeof($usuario)){
            $usuario = new User();
        }
        
        return View::make("usuario.form", array("usuario" => $usuario));
    }
    
    public function crearUsuario(){       
        return $this->mostrarFormUsuario(new User());
    }
    
    public function editarUsuario($id){        
        $usuario = User::find($id);
        if (!sizeof($usuario)){
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }
        
        return $this->mostrarFormUsuario($usuario);
    }
    
    public function guardarUsuario(){        
        $id = Input::get("id");
        $clave = Input::get("password");
        
        $usuario = User::find($id);
        if (!sizeof($usuario)){
            $usuario = new User();
        }
        
        if (empty($id) && empty($clave)){
            Session::flash("mensajeError", "Debe ingresar la clave para el usuario");
            return $this->mostrarFormUsuario($usuario);
        }
        else if (!empty($clave)){
            $clave = Hash::make($clave);
        }
        else{
            $clave = $usuario->password;
        }
        
        $usuario->fill(Input::all());
        $usuario->password = $clave;
        
        $cntLogin = User::where("login", $usuario->login)->where("id", "!=", $usuario->id)->count();
        if ($cntLogin > 0){
            Session::flash("mensajeError", "Ya existe un usuario con el login ingresado");
            return $this->mostrarFormUsuario($usuario);
        }
        
        if ($usuario->save()){
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensaje", "Usuario guardado exitosamente");
        }
        else{
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo guardar el usuario");
        }
    }
    
    public function mostrarFormPermisos($id){
        $usuario = User::find($id);
        if (!sizeof($usuario)){
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }
        
        $controladores = Controlador::with(array('usuarios' => function($query) use($usuario) {
            $query->where('id_usuario', $usuario->id);
        }))->get();
        
        return View::make('usuario.form_permisos', array("usuario" => $usuario, "controladores" => $controladores));
    }
    
    public function guardarPermisos(){
        $usuario = User::find(Input::get("id_usuario"));
        if (!sizeof($usuario)){
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }
        
        $usuario->controladores()->detach();
        $controladores = Input::get("controlador");
        $usuario->controladores()->attach($controladores);
        return Redirect::action('UsuarioController@mostrarIndex')->with("mensaje", "Permisos asignados exitosamente");
    }
}