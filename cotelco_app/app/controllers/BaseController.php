<?php

class BaseController extends Controller {
    function __construct() {        
        $this->beforeFilter(function() {
            list($clase, $accion) = explode("@", Route::currentRouteAction());
            
            if (!Auth::check() && $clase != "SesionController" && $clase != "HomeController"){
                return Redirect::to("/sesion/formLogin")->with("mensajeError", "Necesita estar logueado para ingresar a esta secci&oacute;n");
            }
        });
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
}
