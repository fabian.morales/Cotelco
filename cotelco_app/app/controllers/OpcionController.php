<?php

class OpcionController extends AdminController {
   public function mostrarIndex(){  
        $opciones = Opcion::paginate(20);
        return View::make('opcion.index', array("opciones" => $opciones));
    }
       
    public function mostrarFormOpcion($opcion){       
        if (!sizeof($opcion)){
            $opcion = new Opcion();
        }

        return View::make("opcion.form", array("opcion" => $opcion));
    }
    
    public function crearOpcion(){
        if (Auth::user()->admin != 'Y'){
            return Redirect::to("/")->with("mensajeError", "No tiene permitido el acceso a esta opci&oacute;n");
        }
        
        return $this->mostrarFormOpcion(new Opcion());
    }
    
    public function editarOpcion($id){
        if (Auth::user()->admin != 'Y'){
            return Redirect::to("/")->with("mensajeError", "No tiene permitido el acceso a esta opci&oacute;n");
        }
        
        $opcion = Opcion::find($id);
        if (!sizeof($opcion)){
            return Redirect::action('OpcionController@mostrarIndex')->with("mensajeError", "No se pudo encontrar la opci&oacute;n");
        }
        
        return $this->mostrarFormOpcion($opcion);
    }
    
    public function guardarOpcion(){
        if (Auth::user()->admin != 'Y'){
            return Redirect::to("/")->with("mensajeError", "No tiene permitido el acceso a esta opci&oacute;n");
        }
        
        $id = Input::get("id");        
        
        $opcion = Opcion::find($id);
        if (!sizeof($opcion)){
            $opcion = new Opcion();
        }
        
        $opcion->fill(Input::all());
        
        if ($opcion->save()){
            return Redirect::action('OpcionController@mostrarIndex')->with("mensaje", "Opcion guardada exitosamente");
        }
        else{
            return Redirect::action('OpcionController@mostrarIndex')->with("mensajeError", "No se pudo guardar la opcion");
        }
    }
}