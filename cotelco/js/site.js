(function (window, $){
    $(document).ready(function() {
        $(document).foundation();
        
        $("#lnkSiguiente, #lnkAnterior").click(function(e) {
            e.preventDefault();
            var $destino = $(this).attr("href");
            $.ajax({
                url: $("#form_preguntas").attr("action"),
                data: $("#form_preguntas").serialize(),
                method: 'post',
                success: function(res){
                    if (res === 'ok'){
                        window.location.href = $destino;
                    }
                    else{
                        alert('No se pudo guardar las respuestas');
                    }
                }
            });
        });
        
        $("select#depto, select#depto_repr, select#depto_ins").change(function(e){
            var $ciudad = $("select" + $(this).attr("data-select"));
            
            $.ajax({
                url: $("#url_ciudades").val() + "/" + $(this).val(),
                success: function (json) {
                    /*var json = $.parseJSON(res);*/
                    $ciudad.empty();                    
                    $.each(json, function(i, o) {
                        $ciudad.append("<option value='" + o.nombre + "'>" + o.nombre + "</div>");
                    });
                }
            });
        });
        
        $("#form_registro").validate({
            ignore: "",
            invalidHandler: function(event, validator) {            
                var errors = validator.numberOfInvalids();
                if (errors) {
                    alert('No ha ingresado todos los datos requeridos. Por favor verifique en todas las pestañas que no falte ningún dato.')
                }
            }
        });
        
        $("input[rel='navIns']").click(function(e) {
            e.preventDefault();
            var $target = $(this).attr("data-target");
            $($target).trigger("click");
        });        
        //$("#video_tutorial").fancybox({ type: 'ajax' });
    });
})(window, jQuery); 