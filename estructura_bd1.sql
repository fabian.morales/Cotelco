create table sis_usuario(
    id int auto_increment primary key,
    nombre varchar(200),
    login varchar(100),
    email varchar(200),
    password varchar(100),
	rnt varchar(50),
	ciudad varchar(200),
	direccion varchar(200),
	telefono varchar(20),
	nombre_repr varchar(200),
	cedula_repr varchar(15),
	ciudad_repr varchar(200),
	direccion_repr varchar(200),
	telefono_repr varchar(20),
	celular_repr varchar(20),
	correo_repr varchar(20),
	nombre_ins varchar(200),
	cedula_ins varchar(15),
	cargo_ins varchar(200),
	ciudad_ins varchar(200),
	direccion_ins varchar(200),
	telefono_ins varchar(20),
	celular_ins varchar(20),
	correo_ins varchar(20),	
    admin char(1) default 'N',
    activo char(1) default 'Y',
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_grupo(
    id int auto_increment primary key,
    nivel1 int,
    nivel2 int,
    nivel3 int,
    nivel4 int,
    nivel5 int,
    nombre varchar(200),
    descripcion text,
    orden int,
    id_grupo int default null,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_grupo) references sis_grupo (id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_pregunta(
    id int auto_increment primary key,
    enunciado text,
    id_grupo int,
    orden int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_grupo) references sis_grupo (id)    
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_opcion_pregunta(
    id int auto_increment primary key,
    nombre varchar(200),
    valor int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_respuesta(
    id int auto_increment primary key,
    id_usuario int,
    id_pregunta int,
    id_opcion int,
    observaciones text,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_usuario) references sis_usuario (id),
    foreign key (id_pregunta) references sis_pregunta (id),
    foreign key (id_opcion) references sis_opcion_pregunta (id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_respuesta_mensaje(
    id int auto_increment primary key,
    id_pregunta int,
    id_opcion int,
    mensaje text,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_pregunta) references sis_pregunta (id),
    foreign key (id_opcion) references sis_opcion_pregunta (id)    
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_plan_accion(
    id int auto_increment primary key,
    id_usuario int,
    id_pregunta int,
    accion text,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_usuario) references sis_usuario (id),
    foreign key (id_pregunta) references sis_pregunta (id)    
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE sis_pregunta_map(
	nivel1 INT,
	id_grupo INT NOT NULL DEFAULT '0',
	id_pregunta INT DEFAULT '0',
	mostrar_grupo INT,
	grupo_ant INT NOT NULL DEFAULT '0',
	peso INT NOT NULL DEFAULT '0',
	cnt int, 
	pagina int,
	cambio int,
	nivel_ant int
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE sis_pregunta_nivel_map (
	nivel1 INT NULL DEFAULT NULL,
	pagina_inicio int,
	pagina_fin int
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

create table sis_depto(
    id varchar(5) not null primary key,
    nombre varchar(255) not null,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

create table sis_ciudad(
    id varchar(5) not null primary key,
    nombre varchar(255) not null,
    id_depto varchar(5) not null,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
	foreign key (id_depto) references sis_depto (id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

insert into sis_depto(id, nombre) values('05','ANTIOQUIA');
insert into sis_depto(id, nombre) values('08','ATLANTICO');
insert into sis_depto(id, nombre) values('11','BOGOTA');
insert into sis_depto(id, nombre) values('13','BOLIVAR');
insert into sis_depto(id, nombre) values('15','BOYACA');
insert into sis_depto(id, nombre) values('17','CALDAS');
insert into sis_depto(id, nombre) values('18','CAQUETA');
insert into sis_depto(id, nombre) values('19','CAUCA');
insert into sis_depto(id, nombre) values('20','CESAR');
insert into sis_depto(id, nombre) values('23','CORDOBA');
insert into sis_depto(id, nombre) values('25','CUNDINAMARCA');
insert into sis_depto(id, nombre) values('27','CHOCO');
insert into sis_depto(id, nombre) values('41','HUILA');
insert into sis_depto(id, nombre) values('44','LA GUAJIRA');
insert into sis_depto(id, nombre) values('47','MAGDALENA');
insert into sis_depto(id, nombre) values('50','META');
insert into sis_depto(id, nombre) values('52','NARIÑO');
insert into sis_depto(id, nombre) values('54','N. DE SANTANDER');
insert into sis_depto(id, nombre) values('63','QUINDIO');
insert into sis_depto(id, nombre) values('66','RISARALDA');
insert into sis_depto(id, nombre) values('68','SANTANDER');
insert into sis_depto(id, nombre) values('70','SUCRE');
insert into sis_depto(id, nombre) values('73','TOLIMA');
insert into sis_depto(id, nombre) values('76','VALLE DEL CAUCA');
insert into sis_depto(id, nombre) values('81','ARAUCA');
insert into sis_depto(id, nombre) values('85','CASANARE');
insert into sis_depto(id, nombre) values('86','PUTUMAYO');
insert into sis_depto(id, nombre) values('88','SAN ANDRES');
insert into sis_depto(id, nombre) values('91','AMAZONAS');
insert into sis_depto(id, nombre) values('94','GUAINIA');
insert into sis_depto(id, nombre) values('95','GUAVIARE');
insert into sis_depto(id, nombre) values('97','VAUPES');
insert into sis_depto(id, nombre) values('99','VICHADA');

insert into sis_ciudad(id, id_depto, nombre) values('05001','05', 'MEDELLIN');
insert into sis_ciudad(id, id_depto, nombre) values('05002','05', 'ABEJORRAL');
insert into sis_ciudad(id, id_depto, nombre) values('05004','05', 'ABRIAQUI');
insert into sis_ciudad(id, id_depto, nombre) values('05021','05', 'ALEJANDRIA');
insert into sis_ciudad(id, id_depto, nombre) values('05030','05', 'AMAGA');
insert into sis_ciudad(id, id_depto, nombre) values('05031','05', 'AMALFI');
insert into sis_ciudad(id, id_depto, nombre) values('05034','05', 'ANDES');
insert into sis_ciudad(id, id_depto, nombre) values('05036','05', 'ANGELOPOLIS');
insert into sis_ciudad(id, id_depto, nombre) values('05038','05', 'ANGOSTURA');
insert into sis_ciudad(id, id_depto, nombre) values('05040','05', 'ANORI');
insert into sis_ciudad(id, id_depto, nombre) values('05042','05', 'SANTAFE DE ANTIOQUIA');
insert into sis_ciudad(id, id_depto, nombre) values('05044','05', 'ANZA');
insert into sis_ciudad(id, id_depto, nombre) values('05045','05', 'APARTADO');
insert into sis_ciudad(id, id_depto, nombre) values('05051','05', 'ARBOLETES');
insert into sis_ciudad(id, id_depto, nombre) values('05055','05', 'ARGELIA');
insert into sis_ciudad(id, id_depto, nombre) values('05059','05', 'ARMENIA');
insert into sis_ciudad(id, id_depto, nombre) values('05079','05', 'BARBOSA');
insert into sis_ciudad(id, id_depto, nombre) values('05086','05', 'BELMIRA');
insert into sis_ciudad(id, id_depto, nombre) values('05088','05', 'BELLO');
insert into sis_ciudad(id, id_depto, nombre) values('05091','05', 'BETANIA');
insert into sis_ciudad(id, id_depto, nombre) values('05093','05', 'BETULIA');
insert into sis_ciudad(id, id_depto, nombre) values('05101','05', 'CIUDAD BOLIVAR');
insert into sis_ciudad(id, id_depto, nombre) values('05107','05', 'BRICEÑO');
insert into sis_ciudad(id, id_depto, nombre) values('05113','05', 'BURITICA');
insert into sis_ciudad(id, id_depto, nombre) values('05120','05', 'CACERES');
insert into sis_ciudad(id, id_depto, nombre) values('05125','05', 'CAICEDO');
insert into sis_ciudad(id, id_depto, nombre) values('05129','05', 'CALDAS');
insert into sis_ciudad(id, id_depto, nombre) values('05134','05', 'CAMPAMENTO');
insert into sis_ciudad(id, id_depto, nombre) values('05138','05', 'CAÑASGORDAS');
insert into sis_ciudad(id, id_depto, nombre) values('05142','05', 'CARACOLI');
insert into sis_ciudad(id, id_depto, nombre) values('05145','05', 'CARAMANTA');
insert into sis_ciudad(id, id_depto, nombre) values('05147','05', 'CAREPA');
insert into sis_ciudad(id, id_depto, nombre) values('05148','05', 'EL CARMEN DE VIBORAL');
insert into sis_ciudad(id, id_depto, nombre) values('05150','05', 'CAROLINA');
insert into sis_ciudad(id, id_depto, nombre) values('05154','05', 'CAUCASIA');
insert into sis_ciudad(id, id_depto, nombre) values('05172','05', 'CHIGORODO');
insert into sis_ciudad(id, id_depto, nombre) values('05190','05', 'CISNEROS');
insert into sis_ciudad(id, id_depto, nombre) values('05197','05', 'COCORNA');
insert into sis_ciudad(id, id_depto, nombre) values('05206','05', 'CONCEPCION');
insert into sis_ciudad(id, id_depto, nombre) values('05209','05', 'CONCORDIA');
insert into sis_ciudad(id, id_depto, nombre) values('05212','05', 'COPACABANA');
insert into sis_ciudad(id, id_depto, nombre) values('05234','05', 'DABEIBA');
insert into sis_ciudad(id, id_depto, nombre) values('05237','05', 'DON MATIAS');
insert into sis_ciudad(id, id_depto, nombre) values('05240','05', 'EBEJICO');
insert into sis_ciudad(id, id_depto, nombre) values('05250','05', 'EL BAGRE');
insert into sis_ciudad(id, id_depto, nombre) values('05264','05', 'ENTRERRIOS');
insert into sis_ciudad(id, id_depto, nombre) values('05266','05', 'ENVIGADO');
insert into sis_ciudad(id, id_depto, nombre) values('05282','05', 'FREDONIA');
insert into sis_ciudad(id, id_depto, nombre) values('05284','05', 'FRONTINO');
insert into sis_ciudad(id, id_depto, nombre) values('05306','05', 'GIRALDO');
insert into sis_ciudad(id, id_depto, nombre) values('05308','05', 'GIRARDOTA');
insert into sis_ciudad(id, id_depto, nombre) values('05310','05', 'GOMEZ PLATA');
insert into sis_ciudad(id, id_depto, nombre) values('05313','05', 'GRANADA');
insert into sis_ciudad(id, id_depto, nombre) values('05315','05', 'GUADALUPE');
insert into sis_ciudad(id, id_depto, nombre) values('05318','05', 'GUARNE');
insert into sis_ciudad(id, id_depto, nombre) values('05321','05', 'GUATAPE');
insert into sis_ciudad(id, id_depto, nombre) values('05347','05', 'HELICONIA');
insert into sis_ciudad(id, id_depto, nombre) values('05353','05', 'HISPANIA');
insert into sis_ciudad(id, id_depto, nombre) values('05360','05', 'ITAGUI');
insert into sis_ciudad(id, id_depto, nombre) values('05361','05', 'ITUANGO');
insert into sis_ciudad(id, id_depto, nombre) values('05364','05', 'JARDIN');
insert into sis_ciudad(id, id_depto, nombre) values('05368','05', 'JERICO');
insert into sis_ciudad(id, id_depto, nombre) values('05376','05', 'LA CEJA');
insert into sis_ciudad(id, id_depto, nombre) values('05380','05', 'LA ESTRELLA');
insert into sis_ciudad(id, id_depto, nombre) values('05390','05', 'LA PINTADA');
insert into sis_ciudad(id, id_depto, nombre) values('05400','05', 'LA UNION');
insert into sis_ciudad(id, id_depto, nombre) values('05411','05', 'LIBORINA');
insert into sis_ciudad(id, id_depto, nombre) values('05425','05', 'MACEO');
insert into sis_ciudad(id, id_depto, nombre) values('05440','05', 'MARINILLA');
insert into sis_ciudad(id, id_depto, nombre) values('05467','05', 'MONTEBELLO');
insert into sis_ciudad(id, id_depto, nombre) values('05475','05', 'MURINDO');
insert into sis_ciudad(id, id_depto, nombre) values('05480','05', 'MUTATA');
insert into sis_ciudad(id, id_depto, nombre) values('05483','05', 'NARIÑO');
insert into sis_ciudad(id, id_depto, nombre) values('05490','05', 'NECOCLI');
insert into sis_ciudad(id, id_depto, nombre) values('05495','05', 'NECHI');
insert into sis_ciudad(id, id_depto, nombre) values('05501','05', 'OLAYA');
insert into sis_ciudad(id, id_depto, nombre) values('05541','05', 'PEÐOL');
insert into sis_ciudad(id, id_depto, nombre) values('05543','05', 'PEQUE');
insert into sis_ciudad(id, id_depto, nombre) values('05576','05', 'PUEBLORRICO');
insert into sis_ciudad(id, id_depto, nombre) values('05579','05', 'PUERTO BERRIO');
insert into sis_ciudad(id, id_depto, nombre) values('05585','05', 'PUERTO NARE');
insert into sis_ciudad(id, id_depto, nombre) values('05591','05', 'PUERTO TRIUNFO');
insert into sis_ciudad(id, id_depto, nombre) values('05604','05', 'REMEDIOS');
insert into sis_ciudad(id, id_depto, nombre) values('05607','05', 'RETIRO');
insert into sis_ciudad(id, id_depto, nombre) values('05615','05', 'RIONEGRO');
insert into sis_ciudad(id, id_depto, nombre) values('05628','05', 'SABANALARGA');
insert into sis_ciudad(id, id_depto, nombre) values('05631','05', 'SABANETA');
insert into sis_ciudad(id, id_depto, nombre) values('05642','05', 'SALGAR');
insert into sis_ciudad(id, id_depto, nombre) values('05647','05', 'SAN ANDRES DE CUERQUIA');
insert into sis_ciudad(id, id_depto, nombre) values('05649','05', 'SAN CARLOS');
insert into sis_ciudad(id, id_depto, nombre) values('05652','05', 'SAN FRANCISCO');
insert into sis_ciudad(id, id_depto, nombre) values('05656','05', 'SAN JERONIMO');
insert into sis_ciudad(id, id_depto, nombre) values('05658','05', 'SAN JOSE DE LA MONTAÑA');
insert into sis_ciudad(id, id_depto, nombre) values('05659','05', 'SAN JUAN DE URABA');
insert into sis_ciudad(id, id_depto, nombre) values('05660','05', 'SAN LUIS');
insert into sis_ciudad(id, id_depto, nombre) values('05664','05', 'SAN PEDRO');
insert into sis_ciudad(id, id_depto, nombre) values('05665','05', 'SAN PEDRO DE URABA');
insert into sis_ciudad(id, id_depto, nombre) values('05667','05', 'SAN RAFAEL');
insert into sis_ciudad(id, id_depto, nombre) values('05670','05', 'SAN ROQUE');
insert into sis_ciudad(id, id_depto, nombre) values('05674','05', 'SAN VICENTE');
insert into sis_ciudad(id, id_depto, nombre) values('05679','05', 'SANTA BARBARA');
insert into sis_ciudad(id, id_depto, nombre) values('05686','05', 'SANTA ROSA DE OSOS');
insert into sis_ciudad(id, id_depto, nombre) values('05690','05', 'SANTO DOMINGO');
insert into sis_ciudad(id, id_depto, nombre) values('05697','05', 'EL SANTUARIO');
insert into sis_ciudad(id, id_depto, nombre) values('05736','05', 'SEGOVIA');
insert into sis_ciudad(id, id_depto, nombre) values('05756','05', 'SONSON');
insert into sis_ciudad(id, id_depto, nombre) values('05761','05', 'SOPETRAN');
insert into sis_ciudad(id, id_depto, nombre) values('05789','05', 'TAMESIS');
insert into sis_ciudad(id, id_depto, nombre) values('05790','05', 'TARAZA');
insert into sis_ciudad(id, id_depto, nombre) values('05792','05', 'TARSO');
insert into sis_ciudad(id, id_depto, nombre) values('05809','05', 'TITIRIBI');
insert into sis_ciudad(id, id_depto, nombre) values('05819','05', 'TOLEDO');
insert into sis_ciudad(id, id_depto, nombre) values('05837','05', 'TURBO');
insert into sis_ciudad(id, id_depto, nombre) values('05842','05', 'URAMITA');
insert into sis_ciudad(id, id_depto, nombre) values('05847','05', 'URRAO');
insert into sis_ciudad(id, id_depto, nombre) values('05854','05', 'VALDIVIA');
insert into sis_ciudad(id, id_depto, nombre) values('05856','05', 'VALPARAISO');
insert into sis_ciudad(id, id_depto, nombre) values('05858','05', 'VEGACHI');
insert into sis_ciudad(id, id_depto, nombre) values('05861','05', 'VENECIA');
insert into sis_ciudad(id, id_depto, nombre) values('05873','05', 'VIGIA DEL FUERTE');
insert into sis_ciudad(id, id_depto, nombre) values('05885','05', 'YALI');
insert into sis_ciudad(id, id_depto, nombre) values('05887','05', 'YARUMAL');
insert into sis_ciudad(id, id_depto, nombre) values('05890','05', 'YOLOMBO');
insert into sis_ciudad(id, id_depto, nombre) values('05893','05', 'YONDO');
insert into sis_ciudad(id, id_depto, nombre) values('05895','05', 'ZARAGOZA');
insert into sis_ciudad(id, id_depto, nombre) values('08001','08', 'BARRANQUILLA');
insert into sis_ciudad(id, id_depto, nombre) values('08078','08', 'BARANOA');
insert into sis_ciudad(id, id_depto, nombre) values('08137','08', 'CAMPO DE LA CRUZ');
insert into sis_ciudad(id, id_depto, nombre) values('08141','08', 'CANDELARIA');
insert into sis_ciudad(id, id_depto, nombre) values('08296','08', 'GALAPA');
insert into sis_ciudad(id, id_depto, nombre) values('08372','08', 'JUAN DE ACOSTA');
insert into sis_ciudad(id, id_depto, nombre) values('08421','08', 'LURUACO');
insert into sis_ciudad(id, id_depto, nombre) values('08433','08', 'MALAMBO');
insert into sis_ciudad(id, id_depto, nombre) values('08436','08', 'MANATI');
insert into sis_ciudad(id, id_depto, nombre) values('08520','08', 'PALMAR DE VARELA');
insert into sis_ciudad(id, id_depto, nombre) values('08549','08', 'PIOJO');
insert into sis_ciudad(id, id_depto, nombre) values('08558','08', 'POLONUEVO');
insert into sis_ciudad(id, id_depto, nombre) values('08560','08', 'PONEDERA');
insert into sis_ciudad(id, id_depto, nombre) values('08573','08', 'PUERTO COLOMBIA');
insert into sis_ciudad(id, id_depto, nombre) values('08606','08', 'REPELON');
insert into sis_ciudad(id, id_depto, nombre) values('08634','08', 'SABANAGRANDE');
insert into sis_ciudad(id, id_depto, nombre) values('08638','08', 'SABANALARGA');
insert into sis_ciudad(id, id_depto, nombre) values('08675','08', 'SANTA LUCIA');
insert into sis_ciudad(id, id_depto, nombre) values('08685','08', 'SANTO TOMAS');
insert into sis_ciudad(id, id_depto, nombre) values('08758','08', 'SOLEDAD');
insert into sis_ciudad(id, id_depto, nombre) values('08770','08', 'SUAN');
insert into sis_ciudad(id, id_depto, nombre) values('08832','08', 'TUBARA');
insert into sis_ciudad(id, id_depto, nombre) values('08849','08', 'USIACURI');
insert into sis_ciudad(id, id_depto, nombre) values('11001','11', 'BOGOTA, D.C.');
insert into sis_ciudad(id, id_depto, nombre) values('13001','13', 'CARTAGENA');
insert into sis_ciudad(id, id_depto, nombre) values('13006','13', 'ACHI');
insert into sis_ciudad(id, id_depto, nombre) values('13030','13', 'ALTOS DEL ROSARIO');
insert into sis_ciudad(id, id_depto, nombre) values('13042','13', 'ARENAL');
insert into sis_ciudad(id, id_depto, nombre) values('13052','13', 'ARJONA');
insert into sis_ciudad(id, id_depto, nombre) values('13062','13', 'ARROYOHONDO');
insert into sis_ciudad(id, id_depto, nombre) values('13074','13', 'BARRANCO DE LOBA');
insert into sis_ciudad(id, id_depto, nombre) values('13140','13', 'CALAMAR');
insert into sis_ciudad(id, id_depto, nombre) values('13160','13', 'CANTAGALLO');
insert into sis_ciudad(id, id_depto, nombre) values('13188','13', 'CICUCO');
insert into sis_ciudad(id, id_depto, nombre) values('13212','13', 'CORDOBA');
insert into sis_ciudad(id, id_depto, nombre) values('13222','13', 'CLEMENCIA');
insert into sis_ciudad(id, id_depto, nombre) values('13244','13', 'EL CARMEN DE BOLIVAR');
insert into sis_ciudad(id, id_depto, nombre) values('13248','13', 'EL GUAMO');
insert into sis_ciudad(id, id_depto, nombre) values('13268','13', 'EL PEÑON');
insert into sis_ciudad(id, id_depto, nombre) values('13300','13', 'HATILLO DE LOBA');
insert into sis_ciudad(id, id_depto, nombre) values('13430','13', 'MAGANGUE');
insert into sis_ciudad(id, id_depto, nombre) values('13433','13', 'MAHATES');
insert into sis_ciudad(id, id_depto, nombre) values('13440','13', 'MARGARITA');
insert into sis_ciudad(id, id_depto, nombre) values('13442','13', 'MARIA LA BAJA');
insert into sis_ciudad(id, id_depto, nombre) values('13458','13', 'MONTECRISTO');
insert into sis_ciudad(id, id_depto, nombre) values('13468','13', 'MOMPOS');
insert into sis_ciudad(id, id_depto, nombre) values('13490','13', 'NOROSI');
insert into sis_ciudad(id, id_depto, nombre) values('13473','13', 'MORALES');
insert into sis_ciudad(id, id_depto, nombre) values('13549','13', 'PINILLOS');
insert into sis_ciudad(id, id_depto, nombre) values('13580','13', 'REGIDOR');
insert into sis_ciudad(id, id_depto, nombre) values('13600','13', 'RIO VIEJO');
insert into sis_ciudad(id, id_depto, nombre) values('13620','13', 'SAN CRISTOBAL');
insert into sis_ciudad(id, id_depto, nombre) values('13647','13', 'SAN ESTANISLAO');
insert into sis_ciudad(id, id_depto, nombre) values('13650','13', 'SAN FERNANDO');
insert into sis_ciudad(id, id_depto, nombre) values('13654','13', 'SAN JACINTO');
insert into sis_ciudad(id, id_depto, nombre) values('13655','13', 'SAN JACINTO DEL CAUCA');
insert into sis_ciudad(id, id_depto, nombre) values('13657','13', 'SAN JUAN NEPOMUCENO');
insert into sis_ciudad(id, id_depto, nombre) values('13667','13', 'SAN MARTIN DE LOBA');
insert into sis_ciudad(id, id_depto, nombre) values('13670','13', 'SAN PABLO');
insert into sis_ciudad(id, id_depto, nombre) values('13673','13', 'SANTA CATALINA');
insert into sis_ciudad(id, id_depto, nombre) values('13683','13', 'SANTA ROSA');
insert into sis_ciudad(id, id_depto, nombre) values('13688','13', 'SANTA ROSA DEL SUR');
insert into sis_ciudad(id, id_depto, nombre) values('13744','13', 'SIMITI');
insert into sis_ciudad(id, id_depto, nombre) values('13760','13', 'SOPLAVIENTO');
insert into sis_ciudad(id, id_depto, nombre) values('13780','13', 'TALAIGUA NUEVO');
insert into sis_ciudad(id, id_depto, nombre) values('13810','13', 'TIQUISIO');
insert into sis_ciudad(id, id_depto, nombre) values('13836','13', 'TURBACO');
insert into sis_ciudad(id, id_depto, nombre) values('13838','13', 'TURBANA');
insert into sis_ciudad(id, id_depto, nombre) values('13873','13', 'VILLANUEVA');
insert into sis_ciudad(id, id_depto, nombre) values('13894','13', 'ZAMBRANO');
insert into sis_ciudad(id, id_depto, nombre) values('15001','15', 'TUNJA');
insert into sis_ciudad(id, id_depto, nombre) values('15022','15', 'ALMEIDA');
insert into sis_ciudad(id, id_depto, nombre) values('15047','15', 'AQUITANIA');
insert into sis_ciudad(id, id_depto, nombre) values('15051','15', 'ARCABUCO');
insert into sis_ciudad(id, id_depto, nombre) values('15087','15', 'BELEN');
insert into sis_ciudad(id, id_depto, nombre) values('15090','15', 'BERBEO');
insert into sis_ciudad(id, id_depto, nombre) values('15092','15', 'BETEITIVA');
insert into sis_ciudad(id, id_depto, nombre) values('15097','15', 'BOAVITA');
insert into sis_ciudad(id, id_depto, nombre) values('15104','15', 'BOYACA');
insert into sis_ciudad(id, id_depto, nombre) values('15106','15', 'BRICEÑO');
insert into sis_ciudad(id, id_depto, nombre) values('15109','15', 'BUENAVISTA');
insert into sis_ciudad(id, id_depto, nombre) values('15114','15', 'BUSBANZA');
insert into sis_ciudad(id, id_depto, nombre) values('15131','15', 'CALDAS');
insert into sis_ciudad(id, id_depto, nombre) values('15135','15', 'CAMPOHERMOSO');
insert into sis_ciudad(id, id_depto, nombre) values('15162','15', 'CERINZA');
insert into sis_ciudad(id, id_depto, nombre) values('15172','15', 'CHINAVITA');
insert into sis_ciudad(id, id_depto, nombre) values('15176','15', 'CHIQUINQUIRA');
insert into sis_ciudad(id, id_depto, nombre) values('15180','15', 'CHISCAS');
insert into sis_ciudad(id, id_depto, nombre) values('15183','15', 'CHITA');
insert into sis_ciudad(id, id_depto, nombre) values('15185','15', 'CHITARAQUE');
insert into sis_ciudad(id, id_depto, nombre) values('15187','15', 'CHIVATA');
insert into sis_ciudad(id, id_depto, nombre) values('15189','15', 'CIENEGA');
insert into sis_ciudad(id, id_depto, nombre) values('15204','15', 'COMBITA');
insert into sis_ciudad(id, id_depto, nombre) values('15212','15', 'COPER');
insert into sis_ciudad(id, id_depto, nombre) values('15215','15', 'CORRALES');
insert into sis_ciudad(id, id_depto, nombre) values('15218','15', 'COVARACHIA');
insert into sis_ciudad(id, id_depto, nombre) values('15223','15', 'CUBARA');
insert into sis_ciudad(id, id_depto, nombre) values('15224','15', 'CUCAITA');
insert into sis_ciudad(id, id_depto, nombre) values('15226','15', 'CUITIVA');
insert into sis_ciudad(id, id_depto, nombre) values('15232','15', 'CHIQUIZA');
insert into sis_ciudad(id, id_depto, nombre) values('15236','15', 'CHIVOR');
insert into sis_ciudad(id, id_depto, nombre) values('15238','15', 'DUITAMA');
insert into sis_ciudad(id, id_depto, nombre) values('15244','15', 'EL COCUY');
insert into sis_ciudad(id, id_depto, nombre) values('15248','15', 'EL ESPINO');
insert into sis_ciudad(id, id_depto, nombre) values('15272','15', 'FIRAVITOBA');
insert into sis_ciudad(id, id_depto, nombre) values('15276','15', 'FLORESTA');
insert into sis_ciudad(id, id_depto, nombre) values('15293','15', 'GACHANTIVA');
insert into sis_ciudad(id, id_depto, nombre) values('15296','15', 'GAMEZA');
insert into sis_ciudad(id, id_depto, nombre) values('15299','15', 'GARAGOA');
insert into sis_ciudad(id, id_depto, nombre) values('15317','15', 'GUACAMAYAS');
insert into sis_ciudad(id, id_depto, nombre) values('15322','15', 'GUATEQUE');
insert into sis_ciudad(id, id_depto, nombre) values('15325','15', 'GUAYATA');
insert into sis_ciudad(id, id_depto, nombre) values('15332','15', 'GsICAN');
insert into sis_ciudad(id, id_depto, nombre) values('15362','15', 'IZA');
insert into sis_ciudad(id, id_depto, nombre) values('15367','15', 'JENESANO');
insert into sis_ciudad(id, id_depto, nombre) values('15368','15', 'JERICO');
insert into sis_ciudad(id, id_depto, nombre) values('15377','15', 'LABRANZAGRANDE');
insert into sis_ciudad(id, id_depto, nombre) values('15380','15', 'LA CAPILLA');
insert into sis_ciudad(id, id_depto, nombre) values('15401','15', 'LA VICTORIA');
insert into sis_ciudad(id, id_depto, nombre) values('15403','15', 'LA UVITA');
insert into sis_ciudad(id, id_depto, nombre) values('15407','15', 'VILLA DE LEYVA');
insert into sis_ciudad(id, id_depto, nombre) values('15425','15', 'MACANAL');
insert into sis_ciudad(id, id_depto, nombre) values('15442','15', 'MARIPI');
insert into sis_ciudad(id, id_depto, nombre) values('15455','15', 'MIRAFLORES');
insert into sis_ciudad(id, id_depto, nombre) values('15464','15', 'MONGUA');
insert into sis_ciudad(id, id_depto, nombre) values('15466','15', 'MONGUI');
insert into sis_ciudad(id, id_depto, nombre) values('15469','15', 'MONIQUIRA');
insert into sis_ciudad(id, id_depto, nombre) values('15476','15', 'MOTAVITA');
insert into sis_ciudad(id, id_depto, nombre) values('15480','15', 'MUZO');
insert into sis_ciudad(id, id_depto, nombre) values('15491','15', 'NOBSA');
insert into sis_ciudad(id, id_depto, nombre) values('15494','15', 'NUEVO COLON');
insert into sis_ciudad(id, id_depto, nombre) values('15500','15', 'OICATA');
insert into sis_ciudad(id, id_depto, nombre) values('15507','15', 'OTANCHE');
insert into sis_ciudad(id, id_depto, nombre) values('15511','15', 'PACHAVITA');
insert into sis_ciudad(id, id_depto, nombre) values('15514','15', 'PAEZ');
insert into sis_ciudad(id, id_depto, nombre) values('15516','15', 'PAIPA');
insert into sis_ciudad(id, id_depto, nombre) values('15518','15', 'PAJARITO');
insert into sis_ciudad(id, id_depto, nombre) values('15522','15', 'PANQUEBA');
insert into sis_ciudad(id, id_depto, nombre) values('15531','15', 'PAUNA');
insert into sis_ciudad(id, id_depto, nombre) values('15533','15', 'PAYA');
insert into sis_ciudad(id, id_depto, nombre) values('15537','15', 'PAZ DE RIO');
insert into sis_ciudad(id, id_depto, nombre) values('15542','15', 'PESCA');
insert into sis_ciudad(id, id_depto, nombre) values('15550','15', 'PISBA');
insert into sis_ciudad(id, id_depto, nombre) values('15572','15', 'PUERTO BOYACA');
insert into sis_ciudad(id, id_depto, nombre) values('15580','15', 'QUIPAMA');
insert into sis_ciudad(id, id_depto, nombre) values('15599','15', 'RAMIRIQUI');
insert into sis_ciudad(id, id_depto, nombre) values('15600','15', 'RAQUIRA');
insert into sis_ciudad(id, id_depto, nombre) values('15621','15', 'RONDON');
insert into sis_ciudad(id, id_depto, nombre) values('15632','15', 'SABOYA');
insert into sis_ciudad(id, id_depto, nombre) values('15638','15', 'SACHICA');
insert into sis_ciudad(id, id_depto, nombre) values('15646','15', 'SAMACA');
insert into sis_ciudad(id, id_depto, nombre) values('15660','15', 'SAN EDUARDO');
insert into sis_ciudad(id, id_depto, nombre) values('15664','15', 'SAN JOSE DE PARE');
insert into sis_ciudad(id, id_depto, nombre) values('15667','15', 'SAN LUIS DE GACENO');
insert into sis_ciudad(id, id_depto, nombre) values('15673','15', 'SAN MATEO');
insert into sis_ciudad(id, id_depto, nombre) values('15676','15', 'SAN MIGUEL DE SEMA');
insert into sis_ciudad(id, id_depto, nombre) values('15681','15', 'SAN PABLO DE BORBUR');
insert into sis_ciudad(id, id_depto, nombre) values('15686','15', 'SANTANA');
insert into sis_ciudad(id, id_depto, nombre) values('15690','15', 'SANTA MARIA');
insert into sis_ciudad(id, id_depto, nombre) values('15693','15', 'SANTA ROSA DE VITERBO');
insert into sis_ciudad(id, id_depto, nombre) values('15696','15', 'SANTA SOFIA');
insert into sis_ciudad(id, id_depto, nombre) values('15720','15', 'SATIVANORTE');
insert into sis_ciudad(id, id_depto, nombre) values('15723','15', 'SATIVASUR');
insert into sis_ciudad(id, id_depto, nombre) values('15740','15', 'SIACHOQUE');
insert into sis_ciudad(id, id_depto, nombre) values('15753','15', 'SOATA');
insert into sis_ciudad(id, id_depto, nombre) values('15755','15', 'SOCOTA');
insert into sis_ciudad(id, id_depto, nombre) values('15757','15', 'SOCHA');
insert into sis_ciudad(id, id_depto, nombre) values('15759','15', 'SOGAMOSO');
insert into sis_ciudad(id, id_depto, nombre) values('15761','15', 'SOMONDOCO');
insert into sis_ciudad(id, id_depto, nombre) values('15762','15', 'SORA');
insert into sis_ciudad(id, id_depto, nombre) values('15763','15', 'SOTAQUIRA');
insert into sis_ciudad(id, id_depto, nombre) values('15764','15', 'SORACA');
insert into sis_ciudad(id, id_depto, nombre) values('15774','15', 'SUSACON');
insert into sis_ciudad(id, id_depto, nombre) values('15776','15', 'SUTAMARCHAN');
insert into sis_ciudad(id, id_depto, nombre) values('15778','15', 'SUTATENZA');
insert into sis_ciudad(id, id_depto, nombre) values('15790','15', 'TASCO');
insert into sis_ciudad(id, id_depto, nombre) values('15798','15', 'TENZA');
insert into sis_ciudad(id, id_depto, nombre) values('15804','15', 'TIBANA');
insert into sis_ciudad(id, id_depto, nombre) values('15806','15', 'TIBASOSA');
insert into sis_ciudad(id, id_depto, nombre) values('15808','15', 'TINJACA');
insert into sis_ciudad(id, id_depto, nombre) values('15810','15', 'TIPACOQUE');
insert into sis_ciudad(id, id_depto, nombre) values('15814','15', 'TOCA');
insert into sis_ciudad(id, id_depto, nombre) values('15816','15', 'TOGsI');
insert into sis_ciudad(id, id_depto, nombre) values('15820','15', 'TOPAGA');
insert into sis_ciudad(id, id_depto, nombre) values('15822','15', 'TOTA');
insert into sis_ciudad(id, id_depto, nombre) values('15832','15', 'TUNUNGUA');
insert into sis_ciudad(id, id_depto, nombre) values('15835','15', 'TURMEQUE');
insert into sis_ciudad(id, id_depto, nombre) values('15837','15', 'TUTA');
insert into sis_ciudad(id, id_depto, nombre) values('15839','15', 'TUTAZA');
insert into sis_ciudad(id, id_depto, nombre) values('15842','15', 'UMBITA');
insert into sis_ciudad(id, id_depto, nombre) values('15861','15', 'VENTAQUEMADA');
insert into sis_ciudad(id, id_depto, nombre) values('15879','15', 'VIRACACHA');
insert into sis_ciudad(id, id_depto, nombre) values('15897','15', 'ZETAQUIRA');
insert into sis_ciudad(id, id_depto, nombre) values('17001','17', 'MANIZALES');
insert into sis_ciudad(id, id_depto, nombre) values('17013','17', 'AGUADAS');
insert into sis_ciudad(id, id_depto, nombre) values('17042','17', 'ANSERMA');
insert into sis_ciudad(id, id_depto, nombre) values('17050','17', 'ARANZAZU');
insert into sis_ciudad(id, id_depto, nombre) values('17088','17', 'BELALCAZAR');
insert into sis_ciudad(id, id_depto, nombre) values('17174','17', 'CHINCHINA');
insert into sis_ciudad(id, id_depto, nombre) values('17272','17', 'FILADELFIA');
insert into sis_ciudad(id, id_depto, nombre) values('17380','17', 'LA DORADA');
insert into sis_ciudad(id, id_depto, nombre) values('17388','17', 'LA MERCED');
insert into sis_ciudad(id, id_depto, nombre) values('17433','17', 'MANZANARES');
insert into sis_ciudad(id, id_depto, nombre) values('17442','17', 'MARMATO');
insert into sis_ciudad(id, id_depto, nombre) values('17444','17', 'MARQUETALIA');
insert into sis_ciudad(id, id_depto, nombre) values('17446','17', 'MARULANDA');
insert into sis_ciudad(id, id_depto, nombre) values('17486','17', 'NEIRA');
insert into sis_ciudad(id, id_depto, nombre) values('17495','17', 'NORCASIA');
insert into sis_ciudad(id, id_depto, nombre) values('17513','17', 'PACORA');
insert into sis_ciudad(id, id_depto, nombre) values('17524','17', 'PALESTINA');
insert into sis_ciudad(id, id_depto, nombre) values('17541','17', 'PENSILVANIA');
insert into sis_ciudad(id, id_depto, nombre) values('17614','17', 'RIOSUCIO');
insert into sis_ciudad(id, id_depto, nombre) values('17616','17', 'RISARALDA');
insert into sis_ciudad(id, id_depto, nombre) values('17653','17', 'SALAMINA');
insert into sis_ciudad(id, id_depto, nombre) values('17662','17', 'SAMANA');
insert into sis_ciudad(id, id_depto, nombre) values('17665','17', 'SAN JOSE');
insert into sis_ciudad(id, id_depto, nombre) values('17777','17', 'SUPIA');
insert into sis_ciudad(id, id_depto, nombre) values('17867','17', 'VICTORIA');
insert into sis_ciudad(id, id_depto, nombre) values('17873','17', 'VILLAMARIA');
insert into sis_ciudad(id, id_depto, nombre) values('17877','17', 'VITERBO');
insert into sis_ciudad(id, id_depto, nombre) values('18001','18', 'FLORENCIA');
insert into sis_ciudad(id, id_depto, nombre) values('18029','18', 'ALBANIA');
insert into sis_ciudad(id, id_depto, nombre) values('18094','18', 'BELEN DE LOS ANDAQUIES');
insert into sis_ciudad(id, id_depto, nombre) values('18150','18', 'CARTAGENA DEL CHAIRA');
insert into sis_ciudad(id, id_depto, nombre) values('18205','18', 'CURILLO');
insert into sis_ciudad(id, id_depto, nombre) values('18247','18', 'EL DONCELLO');
insert into sis_ciudad(id, id_depto, nombre) values('18256','18', 'EL PAUJIL');
insert into sis_ciudad(id, id_depto, nombre) values('18410','18', 'LA MONTAÑITA');
insert into sis_ciudad(id, id_depto, nombre) values('18460','18', 'MILAN');
insert into sis_ciudad(id, id_depto, nombre) values('18479','18', 'MORELIA');
insert into sis_ciudad(id, id_depto, nombre) values('18592','18', 'PUERTO RICO');
insert into sis_ciudad(id, id_depto, nombre) values('18610','18', 'SAN JOSE DEL FRAGUA');
insert into sis_ciudad(id, id_depto, nombre) values('18753','18', 'SAN VICENTE DEL CAGUAN');
insert into sis_ciudad(id, id_depto, nombre) values('18756','18', 'SOLANO');
insert into sis_ciudad(id, id_depto, nombre) values('18785','18', 'SOLITA');
insert into sis_ciudad(id, id_depto, nombre) values('18860','18', 'VALPARAISO');
insert into sis_ciudad(id, id_depto, nombre) values('19001','19', 'POPAYAN');
insert into sis_ciudad(id, id_depto, nombre) values('19022','19', 'ALMAGUER');
insert into sis_ciudad(id, id_depto, nombre) values('19050','19', 'ARGELIA');
insert into sis_ciudad(id, id_depto, nombre) values('19075','19', 'BALBOA');
insert into sis_ciudad(id, id_depto, nombre) values('19100','19', 'BOLIVAR');
insert into sis_ciudad(id, id_depto, nombre) values('19110','19', 'BUENOS AIRES');
insert into sis_ciudad(id, id_depto, nombre) values('19130','19', 'CAJIBIO');
insert into sis_ciudad(id, id_depto, nombre) values('19137','19', 'CALDONO');
insert into sis_ciudad(id, id_depto, nombre) values('19142','19', 'CALOTO');
insert into sis_ciudad(id, id_depto, nombre) values('19212','19', 'CORINTO');
insert into sis_ciudad(id, id_depto, nombre) values('19256','19', 'EL TAMBO');
insert into sis_ciudad(id, id_depto, nombre) values('19290','19', 'FLORENCIA');
insert into sis_ciudad(id, id_depto, nombre) values('19300','19', 'GUACHENE');
insert into sis_ciudad(id, id_depto, nombre) values('19318','19', 'GUAPI');
insert into sis_ciudad(id, id_depto, nombre) values('19355','19', 'INZA');
insert into sis_ciudad(id, id_depto, nombre) values('19364','19', 'JAMBALO');
insert into sis_ciudad(id, id_depto, nombre) values('19392','19', 'LA SIERRA');
insert into sis_ciudad(id, id_depto, nombre) values('19397','19', 'LA VEGA');
insert into sis_ciudad(id, id_depto, nombre) values('19418','19', 'LOPEZ');
insert into sis_ciudad(id, id_depto, nombre) values('19450','19', 'MERCADERES');
insert into sis_ciudad(id, id_depto, nombre) values('19455','19', 'MIRANDA');
insert into sis_ciudad(id, id_depto, nombre) values('19473','19', 'MORALES');
insert into sis_ciudad(id, id_depto, nombre) values('19513','19', 'PADILLA');
insert into sis_ciudad(id, id_depto, nombre) values('19517','19', 'PAEZ');
insert into sis_ciudad(id, id_depto, nombre) values('19532','19', 'PATIA');
insert into sis_ciudad(id, id_depto, nombre) values('19533','19', 'PIAMONTE');
insert into sis_ciudad(id, id_depto, nombre) values('19548','19', 'PIENDAMO');
insert into sis_ciudad(id, id_depto, nombre) values('19573','19', 'PUERTO TEJADA');
insert into sis_ciudad(id, id_depto, nombre) values('19585','19', 'PURACE');
insert into sis_ciudad(id, id_depto, nombre) values('19622','19', 'ROSAS');
insert into sis_ciudad(id, id_depto, nombre) values('19693','19', 'SAN SEBASTIAN');
insert into sis_ciudad(id, id_depto, nombre) values('19698','19', 'SANTANDER DE QUILICHAO');
insert into sis_ciudad(id, id_depto, nombre) values('19701','19', 'SANTA ROSA');
insert into sis_ciudad(id, id_depto, nombre) values('19743','19', 'SILVIA');
insert into sis_ciudad(id, id_depto, nombre) values('19760','19', 'SOTARA');
insert into sis_ciudad(id, id_depto, nombre) values('19780','19', 'SUAREZ');
insert into sis_ciudad(id, id_depto, nombre) values('19785','19', 'SUCRE');
insert into sis_ciudad(id, id_depto, nombre) values('19807','19', 'TIMBIO');
insert into sis_ciudad(id, id_depto, nombre) values('19809','19', 'TIMBIQUI');
insert into sis_ciudad(id, id_depto, nombre) values('19821','19', 'TORIBIO');
insert into sis_ciudad(id, id_depto, nombre) values('19824','19', 'TOTORO');
insert into sis_ciudad(id, id_depto, nombre) values('19845','19', 'VILLA RICA');
insert into sis_ciudad(id, id_depto, nombre) values('20001','20', 'VALLEDUPAR');
insert into sis_ciudad(id, id_depto, nombre) values('20011','20', 'AGUACHICA');
insert into sis_ciudad(id, id_depto, nombre) values('20013','20', 'AGUSTIN CODAZZI');
insert into sis_ciudad(id, id_depto, nombre) values('20032','20', 'ASTREA');
insert into sis_ciudad(id, id_depto, nombre) values('20045','20', 'BECERRIL');
insert into sis_ciudad(id, id_depto, nombre) values('20060','20', 'BOSCONIA');
insert into sis_ciudad(id, id_depto, nombre) values('20175','20', 'CHIMICHAGUA');
insert into sis_ciudad(id, id_depto, nombre) values('20178','20', 'CHIRIGUANA');
insert into sis_ciudad(id, id_depto, nombre) values('20228','20', 'CURUMANI');
insert into sis_ciudad(id, id_depto, nombre) values('20238','20', 'EL COPEY');
insert into sis_ciudad(id, id_depto, nombre) values('20250','20', 'EL PASO');
insert into sis_ciudad(id, id_depto, nombre) values('20295','20', 'GAMARRA');
insert into sis_ciudad(id, id_depto, nombre) values('20310','20', 'GONZALEZ');
insert into sis_ciudad(id, id_depto, nombre) values('20383','20', 'LA GLORIA');
insert into sis_ciudad(id, id_depto, nombre) values('20400','20', 'LA JAGUA DE IBIRICO');
insert into sis_ciudad(id, id_depto, nombre) values('20443','20', 'MANAURE');
insert into sis_ciudad(id, id_depto, nombre) values('20517','20', 'PAILITAS');
insert into sis_ciudad(id, id_depto, nombre) values('20550','20', 'PELAYA');
insert into sis_ciudad(id, id_depto, nombre) values('20570','20', 'PUEBLO BELLO');
insert into sis_ciudad(id, id_depto, nombre) values('20614','20', 'RIO DE ORO');
insert into sis_ciudad(id, id_depto, nombre) values('20621','20', 'LA PAZ');
insert into sis_ciudad(id, id_depto, nombre) values('20710','20', 'SAN ALBERTO');
insert into sis_ciudad(id, id_depto, nombre) values('20750','20', 'SAN DIEGO');
insert into sis_ciudad(id, id_depto, nombre) values('20770','20', 'SAN MARTIN');
insert into sis_ciudad(id, id_depto, nombre) values('20787','20', 'TAMALAMEQUE');
insert into sis_ciudad(id, id_depto, nombre) values('23001','23', 'MONTERIA');
insert into sis_ciudad(id, id_depto, nombre) values('23068','23', 'AYAPEL');
insert into sis_ciudad(id, id_depto, nombre) values('23079','23', 'BUENAVISTA');
insert into sis_ciudad(id, id_depto, nombre) values('23090','23', 'CANALETE');
insert into sis_ciudad(id, id_depto, nombre) values('23162','23', 'CERETE');
insert into sis_ciudad(id, id_depto, nombre) values('23168','23', 'CHIMA');
insert into sis_ciudad(id, id_depto, nombre) values('23182','23', 'CHINU');
insert into sis_ciudad(id, id_depto, nombre) values('23189','23', 'CIENAGA DE ORO');
insert into sis_ciudad(id, id_depto, nombre) values('23300','23', 'COTORRA');
insert into sis_ciudad(id, id_depto, nombre) values('23350','23', 'LA APARTADA');
insert into sis_ciudad(id, id_depto, nombre) values('23417','23', 'LORICA');
insert into sis_ciudad(id, id_depto, nombre) values('23419','23', 'LOS CORDOBAS');
insert into sis_ciudad(id, id_depto, nombre) values('23464','23', 'MOMIL');
insert into sis_ciudad(id, id_depto, nombre) values('23466','23', 'MONTELIBANO');
insert into sis_ciudad(id, id_depto, nombre) values('23500','23', 'MOÑITOS');
insert into sis_ciudad(id, id_depto, nombre) values('23555','23', 'PLANETA RICA');
insert into sis_ciudad(id, id_depto, nombre) values('23570','23', 'PUEBLO NUEVO');
insert into sis_ciudad(id, id_depto, nombre) values('23574','23', 'PUERTO ESCONDIDO');
insert into sis_ciudad(id, id_depto, nombre) values('23580','23', 'PUERTO LIBERTADOR');
insert into sis_ciudad(id, id_depto, nombre) values('23586','23', 'PURISIMA');
insert into sis_ciudad(id, id_depto, nombre) values('23660','23', 'SAHAGUN');
insert into sis_ciudad(id, id_depto, nombre) values('23670','23', 'SAN ANDRES SOTAVENTO');
insert into sis_ciudad(id, id_depto, nombre) values('23672','23', 'SAN ANTERO');
insert into sis_ciudad(id, id_depto, nombre) values('23675','23', 'SAN BERNARDO DEL VIENTO');
insert into sis_ciudad(id, id_depto, nombre) values('23678','23', 'SAN CARLOS');
insert into sis_ciudad(id, id_depto, nombre) values('23686','23', 'SAN PELAYO');
insert into sis_ciudad(id, id_depto, nombre) values('23807','23', 'TIERRALTA');
insert into sis_ciudad(id, id_depto, nombre) values('23855','23', 'VALENCIA');
insert into sis_ciudad(id, id_depto, nombre) values('25001','25', 'AGUA DE DIOS');
insert into sis_ciudad(id, id_depto, nombre) values('25019','25', 'ALBAN');
insert into sis_ciudad(id, id_depto, nombre) values('25035','25', 'ANAPOIMA');
insert into sis_ciudad(id, id_depto, nombre) values('25040','25', 'ANOLAIMA');
insert into sis_ciudad(id, id_depto, nombre) values('25053','25', 'ARBELAEZ');
insert into sis_ciudad(id, id_depto, nombre) values('25086','25', 'BELTRAN');
insert into sis_ciudad(id, id_depto, nombre) values('25095','25', 'BITUIMA');
insert into sis_ciudad(id, id_depto, nombre) values('25099','25', 'BOJACA');
insert into sis_ciudad(id, id_depto, nombre) values('25120','25', 'CABRERA');
insert into sis_ciudad(id, id_depto, nombre) values('25123','25', 'CACHIPAY');
insert into sis_ciudad(id, id_depto, nombre) values('25126','25', 'CAJICA');
insert into sis_ciudad(id, id_depto, nombre) values('25148','25', 'CAPARRAPI');
insert into sis_ciudad(id, id_depto, nombre) values('25151','25', 'CAQUEZA');
insert into sis_ciudad(id, id_depto, nombre) values('25154','25', 'CARMEN DE CARUPA');
insert into sis_ciudad(id, id_depto, nombre) values('25168','25', 'CHAGUANI');
insert into sis_ciudad(id, id_depto, nombre) values('25175','25', 'CHIA');
insert into sis_ciudad(id, id_depto, nombre) values('25178','25', 'CHIPAQUE');
insert into sis_ciudad(id, id_depto, nombre) values('25181','25', 'CHOACHI');
insert into sis_ciudad(id, id_depto, nombre) values('25183','25', 'CHOCONTA');
insert into sis_ciudad(id, id_depto, nombre) values('25200','25', 'COGUA');
insert into sis_ciudad(id, id_depto, nombre) values('25214','25', 'COTA');
insert into sis_ciudad(id, id_depto, nombre) values('25224','25', 'CUCUNUBA');
insert into sis_ciudad(id, id_depto, nombre) values('25245','25', 'EL COLEGIO');
insert into sis_ciudad(id, id_depto, nombre) values('25258','25', 'EL PEÑON');
insert into sis_ciudad(id, id_depto, nombre) values('25260','25', 'EL ROSAL');
insert into sis_ciudad(id, id_depto, nombre) values('25269','25', 'FACATATIVA');
insert into sis_ciudad(id, id_depto, nombre) values('25279','25', 'FOMEQUE');
insert into sis_ciudad(id, id_depto, nombre) values('25281','25', 'FOSCA');
insert into sis_ciudad(id, id_depto, nombre) values('25286','25', 'FUNZA');
insert into sis_ciudad(id, id_depto, nombre) values('25288','25', 'FUQUENE');
insert into sis_ciudad(id, id_depto, nombre) values('25290','25', 'FUSAGASUGA');
insert into sis_ciudad(id, id_depto, nombre) values('25293','25', 'GACHALA');
insert into sis_ciudad(id, id_depto, nombre) values('25295','25', 'GACHANCIPA');
insert into sis_ciudad(id, id_depto, nombre) values('25297','25', 'GACHETA');
insert into sis_ciudad(id, id_depto, nombre) values('25299','25', 'GAMA');
insert into sis_ciudad(id, id_depto, nombre) values('25307','25', 'GIRARDOT');
insert into sis_ciudad(id, id_depto, nombre) values('25312','25', 'GRANADA');
insert into sis_ciudad(id, id_depto, nombre) values('25317','25', 'GUACHETA');
insert into sis_ciudad(id, id_depto, nombre) values('25320','25', 'GUADUAS');
insert into sis_ciudad(id, id_depto, nombre) values('25322','25', 'GUASCA');
insert into sis_ciudad(id, id_depto, nombre) values('25324','25', 'GUATAQUI');
insert into sis_ciudad(id, id_depto, nombre) values('25326','25', 'GUATAVITA');
insert into sis_ciudad(id, id_depto, nombre) values('25328','25', 'GUAYABAL DE SIQUIMA');
insert into sis_ciudad(id, id_depto, nombre) values('25335','25', 'GUAYABETAL');
insert into sis_ciudad(id, id_depto, nombre) values('25339','25', 'GUTIERREZ');
insert into sis_ciudad(id, id_depto, nombre) values('25368','25', 'JERUSALEN');
insert into sis_ciudad(id, id_depto, nombre) values('25372','25', 'JUNIN');
insert into sis_ciudad(id, id_depto, nombre) values('25377','25', 'LA CALERA');
insert into sis_ciudad(id, id_depto, nombre) values('25386','25', 'LA MESA');
insert into sis_ciudad(id, id_depto, nombre) values('25394','25', 'LA PALMA');
insert into sis_ciudad(id, id_depto, nombre) values('25398','25', 'LA PEÑA');
insert into sis_ciudad(id, id_depto, nombre) values('25402','25', 'LA VEGA');
insert into sis_ciudad(id, id_depto, nombre) values('25407','25', 'LENGUAZAQUE');
insert into sis_ciudad(id, id_depto, nombre) values('25426','25', 'MACHETA');
insert into sis_ciudad(id, id_depto, nombre) values('25430','25', 'MADRID');
insert into sis_ciudad(id, id_depto, nombre) values('25436','25', 'MANTA');
insert into sis_ciudad(id, id_depto, nombre) values('25438','25', 'MEDINA');
insert into sis_ciudad(id, id_depto, nombre) values('25473','25', 'MOSQUERA');
insert into sis_ciudad(id, id_depto, nombre) values('25483','25', 'NARIÑO');
insert into sis_ciudad(id, id_depto, nombre) values('25486','25', 'NEMOCON');
insert into sis_ciudad(id, id_depto, nombre) values('25488','25', 'NILO');
insert into sis_ciudad(id, id_depto, nombre) values('25489','25', 'NIMAIMA');
insert into sis_ciudad(id, id_depto, nombre) values('25491','25', 'NOCAIMA');
insert into sis_ciudad(id, id_depto, nombre) values('25506','25', 'VENECIA');
insert into sis_ciudad(id, id_depto, nombre) values('25513','25', 'PACHO');
insert into sis_ciudad(id, id_depto, nombre) values('25518','25', 'PAIME');
insert into sis_ciudad(id, id_depto, nombre) values('25524','25', 'PANDI');
insert into sis_ciudad(id, id_depto, nombre) values('25530','25', 'PARATEBUENO');
insert into sis_ciudad(id, id_depto, nombre) values('25535','25', 'PASCA');
insert into sis_ciudad(id, id_depto, nombre) values('25572','25', 'PUERTO SALGAR');
insert into sis_ciudad(id, id_depto, nombre) values('25580','25', 'PULI');
insert into sis_ciudad(id, id_depto, nombre) values('25592','25', 'QUEBRADANEGRA');
insert into sis_ciudad(id, id_depto, nombre) values('25594','25', 'QUETAME');
insert into sis_ciudad(id, id_depto, nombre) values('25596','25', 'QUIPILE');
insert into sis_ciudad(id, id_depto, nombre) values('25599','25', 'APULO');
insert into sis_ciudad(id, id_depto, nombre) values('25612','25', 'RICAURTE');
insert into sis_ciudad(id, id_depto, nombre) values('25645','25', 'SAN ANTONIO DEL TEQUENDAMA');
insert into sis_ciudad(id, id_depto, nombre) values('25649','25', 'SAN BERNARDO');
insert into sis_ciudad(id, id_depto, nombre) values('25653','25', 'SAN CAYETANO');
insert into sis_ciudad(id, id_depto, nombre) values('25658','25', 'SAN FRANCISCO');
insert into sis_ciudad(id, id_depto, nombre) values('25662','25', 'SAN JUAN DE RIO SECO');
insert into sis_ciudad(id, id_depto, nombre) values('25718','25', 'SASAIMA');
insert into sis_ciudad(id, id_depto, nombre) values('25736','25', 'SESQUILE');
insert into sis_ciudad(id, id_depto, nombre) values('25740','25', 'SIBATE');
insert into sis_ciudad(id, id_depto, nombre) values('25743','25', 'SILVANIA');
insert into sis_ciudad(id, id_depto, nombre) values('25745','25', 'SIMIJACA');
insert into sis_ciudad(id, id_depto, nombre) values('25754','25', 'SOACHA');
insert into sis_ciudad(id, id_depto, nombre) values('25758','25', 'SOPO');
insert into sis_ciudad(id, id_depto, nombre) values('25769','25', 'SUBACHOQUE');
insert into sis_ciudad(id, id_depto, nombre) values('25772','25', 'SUESCA');
insert into sis_ciudad(id, id_depto, nombre) values('25777','25', 'SUPATA');
insert into sis_ciudad(id, id_depto, nombre) values('25779','25', 'SUSA');
insert into sis_ciudad(id, id_depto, nombre) values('25781','25', 'SUTATAUSA');
insert into sis_ciudad(id, id_depto, nombre) values('25785','25', 'TABIO');
insert into sis_ciudad(id, id_depto, nombre) values('25793','25', 'TAUSA');
insert into sis_ciudad(id, id_depto, nombre) values('25797','25', 'TENA');
insert into sis_ciudad(id, id_depto, nombre) values('25799','25', 'TENJO');
insert into sis_ciudad(id, id_depto, nombre) values('25805','25', 'TIBACUY');
insert into sis_ciudad(id, id_depto, nombre) values('25807','25', 'TIBIRITA');
insert into sis_ciudad(id, id_depto, nombre) values('25815','25', 'TOCAIMA');
insert into sis_ciudad(id, id_depto, nombre) values('25817','25', 'TOCANCIPA');
insert into sis_ciudad(id, id_depto, nombre) values('25823','25', 'TOPAIPI');
insert into sis_ciudad(id, id_depto, nombre) values('25839','25', 'UBALA');
insert into sis_ciudad(id, id_depto, nombre) values('25841','25', 'UBAQUE');
insert into sis_ciudad(id, id_depto, nombre) values('25843','25', 'VILLA DE SAN DIEGO DE UBATE');
insert into sis_ciudad(id, id_depto, nombre) values('25845','25', 'UNE');
insert into sis_ciudad(id, id_depto, nombre) values('25851','25', 'UTICA');
insert into sis_ciudad(id, id_depto, nombre) values('25862','25', 'VERGARA');
insert into sis_ciudad(id, id_depto, nombre) values('25867','25', 'VIANI');
insert into sis_ciudad(id, id_depto, nombre) values('25871','25', 'VILLAGOMEZ');
insert into sis_ciudad(id, id_depto, nombre) values('25873','25', 'VILLAPINZON');
insert into sis_ciudad(id, id_depto, nombre) values('25875','25', 'VILLETA');
insert into sis_ciudad(id, id_depto, nombre) values('25878','25', 'VIOTA');
insert into sis_ciudad(id, id_depto, nombre) values('25885','25', 'YACOPI');
insert into sis_ciudad(id, id_depto, nombre) values('25898','25', 'ZIPACON');
insert into sis_ciudad(id, id_depto, nombre) values('25899','25', 'ZIPAQUIRA');
insert into sis_ciudad(id, id_depto, nombre) values('27001','27', 'QUIBDO');
insert into sis_ciudad(id, id_depto, nombre) values('27006','27', 'ACANDI');
insert into sis_ciudad(id, id_depto, nombre) values('27025','27', 'ALTO BAUDO');
insert into sis_ciudad(id, id_depto, nombre) values('27050','27', 'ATRATO');
insert into sis_ciudad(id, id_depto, nombre) values('27073','27', 'BAGADO');
insert into sis_ciudad(id, id_depto, nombre) values('27075','27', 'BAHIA SOLANO');
insert into sis_ciudad(id, id_depto, nombre) values('27077','27', 'BAJO BAUDO');
insert into sis_ciudad(id, id_depto, nombre) values('27099','27', 'BOJAYA');
insert into sis_ciudad(id, id_depto, nombre) values('27135','27', 'EL CANTON DEL SAN PABLO');
insert into sis_ciudad(id, id_depto, nombre) values('27150','27', 'CARMEN DEL DARIEN');
insert into sis_ciudad(id, id_depto, nombre) values('27160','27', 'CERTEGUI');
insert into sis_ciudad(id, id_depto, nombre) values('27205','27', 'CONDOTO');
insert into sis_ciudad(id, id_depto, nombre) values('27245','27', 'EL CARMEN DE ATRATO');
insert into sis_ciudad(id, id_depto, nombre) values('27250','27', 'EL LITORAL DEL SAN JUAN');
insert into sis_ciudad(id, id_depto, nombre) values('27361','27', 'ISTMINA');
insert into sis_ciudad(id, id_depto, nombre) values('27372','27', 'JURADO');
insert into sis_ciudad(id, id_depto, nombre) values('27413','27', 'LLORO');
insert into sis_ciudad(id, id_depto, nombre) values('27425','27', 'MEDIO ATRATO');
insert into sis_ciudad(id, id_depto, nombre) values('27430','27', 'MEDIO BAUDO');
insert into sis_ciudad(id, id_depto, nombre) values('27450','27', 'MEDIO SAN JUAN');
insert into sis_ciudad(id, id_depto, nombre) values('27491','27', 'NOVITA');
insert into sis_ciudad(id, id_depto, nombre) values('27495','27', 'NUQUI');
insert into sis_ciudad(id, id_depto, nombre) values('27580','27', 'RIO IRO');
insert into sis_ciudad(id, id_depto, nombre) values('27600','27', 'RIO QUITO');
insert into sis_ciudad(id, id_depto, nombre) values('27615','27', 'RIOSUCIO');
insert into sis_ciudad(id, id_depto, nombre) values('27660','27', 'SAN JOSE DEL PALMAR');
insert into sis_ciudad(id, id_depto, nombre) values('27745','27', 'SIPI');
insert into sis_ciudad(id, id_depto, nombre) values('27787','27', 'TADO');
insert into sis_ciudad(id, id_depto, nombre) values('27800','27', 'UNGUIA');
insert into sis_ciudad(id, id_depto, nombre) values('27810','27', 'UNION PANAMERICANA');
insert into sis_ciudad(id, id_depto, nombre) values('41001','41', 'NEIVA');
insert into sis_ciudad(id, id_depto, nombre) values('41006','41', 'ACEVEDO');
insert into sis_ciudad(id, id_depto, nombre) values('41013','41', 'AGRADO');
insert into sis_ciudad(id, id_depto, nombre) values('41016','41', 'AIPE');
insert into sis_ciudad(id, id_depto, nombre) values('41020','41', 'ALGECIRAS');
insert into sis_ciudad(id, id_depto, nombre) values('41026','41', 'ALTAMIRA');
insert into sis_ciudad(id, id_depto, nombre) values('41078','41', 'BARAYA');
insert into sis_ciudad(id, id_depto, nombre) values('41132','41', 'CAMPOALEGRE');
insert into sis_ciudad(id, id_depto, nombre) values('41206','41', 'COLOMBIA');
insert into sis_ciudad(id, id_depto, nombre) values('41244','41', 'ELIAS');
insert into sis_ciudad(id, id_depto, nombre) values('41298','41', 'GARZON');
insert into sis_ciudad(id, id_depto, nombre) values('41306','41', 'GIGANTE');
insert into sis_ciudad(id, id_depto, nombre) values('41319','41', 'GUADALUPE');
insert into sis_ciudad(id, id_depto, nombre) values('41349','41', 'HOBO');
insert into sis_ciudad(id, id_depto, nombre) values('41357','41', 'IQUIRA');
insert into sis_ciudad(id, id_depto, nombre) values('41359','41', 'ISNOS');
insert into sis_ciudad(id, id_depto, nombre) values('41378','41', 'LA ARGENTINA');
insert into sis_ciudad(id, id_depto, nombre) values('41396','41', 'LA PLATA');
insert into sis_ciudad(id, id_depto, nombre) values('41483','41', 'NATAGA');
insert into sis_ciudad(id, id_depto, nombre) values('41503','41', 'OPORAPA');
insert into sis_ciudad(id, id_depto, nombre) values('41518','41', 'PAICOL');
insert into sis_ciudad(id, id_depto, nombre) values('41524','41', 'PALERMO');
insert into sis_ciudad(id, id_depto, nombre) values('41530','41', 'PALESTINA');
insert into sis_ciudad(id, id_depto, nombre) values('41548','41', 'PITAL');
insert into sis_ciudad(id, id_depto, nombre) values('41551','41', 'PITALITO');
insert into sis_ciudad(id, id_depto, nombre) values('41615','41', 'RIVERA');
insert into sis_ciudad(id, id_depto, nombre) values('41660','41', 'SALADOBLANCO');
insert into sis_ciudad(id, id_depto, nombre) values('41668','41', 'SAN AGUSTIN');
insert into sis_ciudad(id, id_depto, nombre) values('41676','41', 'SANTA MARIA');
insert into sis_ciudad(id, id_depto, nombre) values('41770','41', 'SUAZA');
insert into sis_ciudad(id, id_depto, nombre) values('41791','41', 'TARQUI');
insert into sis_ciudad(id, id_depto, nombre) values('41797','41', 'TESALIA');
insert into sis_ciudad(id, id_depto, nombre) values('41799','41', 'TELLO');
insert into sis_ciudad(id, id_depto, nombre) values('41801','41', 'TERUEL');
insert into sis_ciudad(id, id_depto, nombre) values('41807','41', 'TIMANA');
insert into sis_ciudad(id, id_depto, nombre) values('41872','41', 'VILLAVIEJA');
insert into sis_ciudad(id, id_depto, nombre) values('41885','41', 'YAGUARA');
insert into sis_ciudad(id, id_depto, nombre) values('44001','44', 'RIOHACHA');
insert into sis_ciudad(id, id_depto, nombre) values('44035','44', 'ALBANIA');
insert into sis_ciudad(id, id_depto, nombre) values('44078','44', 'BARRANCAS');
insert into sis_ciudad(id, id_depto, nombre) values('44090','44', 'DIBULLA');
insert into sis_ciudad(id, id_depto, nombre) values('44098','44', 'DISTRACCION');
insert into sis_ciudad(id, id_depto, nombre) values('44110','44', 'EL MOLINO');
insert into sis_ciudad(id, id_depto, nombre) values('44279','44', 'FONSECA');
insert into sis_ciudad(id, id_depto, nombre) values('44378','44', 'HATONUEVO');
insert into sis_ciudad(id, id_depto, nombre) values('44420','44', 'LA JAGUA DEL PILAR');
insert into sis_ciudad(id, id_depto, nombre) values('44430','44', 'MAICAO');
insert into sis_ciudad(id, id_depto, nombre) values('44560','44', 'MANAURE');
insert into sis_ciudad(id, id_depto, nombre) values('44650','44', 'SAN JUAN DEL CESAR');
insert into sis_ciudad(id, id_depto, nombre) values('44847','44', 'URIBIA');
insert into sis_ciudad(id, id_depto, nombre) values('44855','44', 'URUMITA');
insert into sis_ciudad(id, id_depto, nombre) values('44874','44', 'VILLANUEVA');
insert into sis_ciudad(id, id_depto, nombre) values('47001','47', 'SANTA MARTA');
insert into sis_ciudad(id, id_depto, nombre) values('47030','47', 'ALGARROBO');
insert into sis_ciudad(id, id_depto, nombre) values('47053','47', 'ARACATACA');
insert into sis_ciudad(id, id_depto, nombre) values('47058','47', 'ARIGUANI');
insert into sis_ciudad(id, id_depto, nombre) values('47161','47', 'CERRO SAN ANTONIO');
insert into sis_ciudad(id, id_depto, nombre) values('47170','47', 'CHIBOLO');
insert into sis_ciudad(id, id_depto, nombre) values('47189','47', 'CIENAGA');
insert into sis_ciudad(id, id_depto, nombre) values('47205','47', 'CONCORDIA');
insert into sis_ciudad(id, id_depto, nombre) values('47245','47', 'EL BANCO');
insert into sis_ciudad(id, id_depto, nombre) values('47258','47', 'EL PIÑON');
insert into sis_ciudad(id, id_depto, nombre) values('47268','47', 'EL RETEN');
insert into sis_ciudad(id, id_depto, nombre) values('47288','47', 'FUNDACION');
insert into sis_ciudad(id, id_depto, nombre) values('47318','47', 'GUAMAL');
insert into sis_ciudad(id, id_depto, nombre) values('47460','47', 'NUEVA GRANADA');
insert into sis_ciudad(id, id_depto, nombre) values('47541','47', 'PEDRAZA');
insert into sis_ciudad(id, id_depto, nombre) values('47545','47', 'PIJIÑO DEL CARMEN');
insert into sis_ciudad(id, id_depto, nombre) values('47551','47', 'PIVIJAY');
insert into sis_ciudad(id, id_depto, nombre) values('47555','47', 'PLATO');
insert into sis_ciudad(id, id_depto, nombre) values('47570','47', 'PUEBLOVIEJO');
insert into sis_ciudad(id, id_depto, nombre) values('47605','47', 'REMOLINO');
insert into sis_ciudad(id, id_depto, nombre) values('47660','47', 'SABANAS DE SAN ANGEL');
insert into sis_ciudad(id, id_depto, nombre) values('47675','47', 'SALAMINA');
insert into sis_ciudad(id, id_depto, nombre) values('47692','47', 'SAN SEBASTIAN DE BUENAVISTA');
insert into sis_ciudad(id, id_depto, nombre) values('47703','47', 'SAN ZENON');
insert into sis_ciudad(id, id_depto, nombre) values('47707','47', 'SANTA ANA');
insert into sis_ciudad(id, id_depto, nombre) values('47720','47', 'SANTA BARBARA DE PINTO');
insert into sis_ciudad(id, id_depto, nombre) values('47745','47', 'SITIONUEVO');
insert into sis_ciudad(id, id_depto, nombre) values('47798','47', 'TENERIFE');
insert into sis_ciudad(id, id_depto, nombre) values('47960','47', 'ZAPAYAN');
insert into sis_ciudad(id, id_depto, nombre) values('47980','47', 'ZONA BANANERA');
insert into sis_ciudad(id, id_depto, nombre) values('50001','50', 'VILLAVICENCIO');
insert into sis_ciudad(id, id_depto, nombre) values('50006','50', 'ACACIAS');
insert into sis_ciudad(id, id_depto, nombre) values('50110','50', 'BARRANCA DE UPIA');
insert into sis_ciudad(id, id_depto, nombre) values('50124','50', 'CABUYARO');
insert into sis_ciudad(id, id_depto, nombre) values('50150','50', 'CASTILLA LA NUEVA');
insert into sis_ciudad(id, id_depto, nombre) values('50223','50', 'CUBARRAL');
insert into sis_ciudad(id, id_depto, nombre) values('50226','50', 'CUMARAL');
insert into sis_ciudad(id, id_depto, nombre) values('50245','50', 'EL CALVARIO');
insert into sis_ciudad(id, id_depto, nombre) values('50251','50', 'EL CASTILLO');
insert into sis_ciudad(id, id_depto, nombre) values('50270','50', 'EL DORADO');
insert into sis_ciudad(id, id_depto, nombre) values('50287','50', 'FUENTE DE ORO');
insert into sis_ciudad(id, id_depto, nombre) values('50313','50', 'GRANADA');
insert into sis_ciudad(id, id_depto, nombre) values('50318','50', 'GUAMAL');
insert into sis_ciudad(id, id_depto, nombre) values('50325','50', 'MAPIRIPAN');
insert into sis_ciudad(id, id_depto, nombre) values('50330','50', 'MESETAS');
insert into sis_ciudad(id, id_depto, nombre) values('50350','50', 'LA MACARENA');
insert into sis_ciudad(id, id_depto, nombre) values('50370','50', 'URIBE');
insert into sis_ciudad(id, id_depto, nombre) values('50400','50', 'LEJANIAS');
insert into sis_ciudad(id, id_depto, nombre) values('50450','50', 'PUERTO CONCORDIA');
insert into sis_ciudad(id, id_depto, nombre) values('50568','50', 'PUERTO GAITAN');
insert into sis_ciudad(id, id_depto, nombre) values('50573','50', 'PUERTO LOPEZ');
insert into sis_ciudad(id, id_depto, nombre) values('50577','50', 'PUERTO LLERAS');
insert into sis_ciudad(id, id_depto, nombre) values('50590','50', 'PUERTO RICO');
insert into sis_ciudad(id, id_depto, nombre) values('50606','50', 'RESTREPO');
insert into sis_ciudad(id, id_depto, nombre) values('50680','50', 'SAN CARLOS DE GUAROA');
insert into sis_ciudad(id, id_depto, nombre) values('50683','50', 'SAN JUAN DE ARAMA');
insert into sis_ciudad(id, id_depto, nombre) values('50686','50', 'SAN JUANITO');
insert into sis_ciudad(id, id_depto, nombre) values('50689','50', 'SAN MARTIN');
insert into sis_ciudad(id, id_depto, nombre) values('50711','50', 'VISTAHERMOSA');
insert into sis_ciudad(id, id_depto, nombre) values('52001','52', 'PASTO');
insert into sis_ciudad(id, id_depto, nombre) values('52019','52', 'ALBAN');
insert into sis_ciudad(id, id_depto, nombre) values('52022','52', 'ALDANA');
insert into sis_ciudad(id, id_depto, nombre) values('52036','52', 'ANCUYA');
insert into sis_ciudad(id, id_depto, nombre) values('52051','52', 'ARBOLEDA');
insert into sis_ciudad(id, id_depto, nombre) values('52079','52', 'BARBACOAS');
insert into sis_ciudad(id, id_depto, nombre) values('52083','52', 'BELEN');
insert into sis_ciudad(id, id_depto, nombre) values('52110','52', 'BUESACO');
insert into sis_ciudad(id, id_depto, nombre) values('52203','52', 'COLON');
insert into sis_ciudad(id, id_depto, nombre) values('52207','52', 'CONSACA');
insert into sis_ciudad(id, id_depto, nombre) values('52210','52', 'CONTADERO');
insert into sis_ciudad(id, id_depto, nombre) values('52215','52', 'CORDOBA');
insert into sis_ciudad(id, id_depto, nombre) values('52224','52', 'CUASPUD');
insert into sis_ciudad(id, id_depto, nombre) values('52227','52', 'CUMBAL');
insert into sis_ciudad(id, id_depto, nombre) values('52233','52', 'CUMBITARA');
insert into sis_ciudad(id, id_depto, nombre) values('52240','52', 'CHACHAGsI');
insert into sis_ciudad(id, id_depto, nombre) values('52250','52', 'EL CHARCO');
insert into sis_ciudad(id, id_depto, nombre) values('52254','52', 'EL PEÑOL');
insert into sis_ciudad(id, id_depto, nombre) values('52256','52', 'EL ROSARIO');
insert into sis_ciudad(id, id_depto, nombre) values('52258','52', 'EL TABLON DE GOMEZ');
insert into sis_ciudad(id, id_depto, nombre) values('52260','52', 'EL TAMBO');
insert into sis_ciudad(id, id_depto, nombre) values('52287','52', 'FUNES');
insert into sis_ciudad(id, id_depto, nombre) values('52317','52', 'GUACHUCAL');
insert into sis_ciudad(id, id_depto, nombre) values('52320','52', 'GUAITARILLA');
insert into sis_ciudad(id, id_depto, nombre) values('52323','52', 'GUALMATAN');
insert into sis_ciudad(id, id_depto, nombre) values('52352','52', 'ILES');
insert into sis_ciudad(id, id_depto, nombre) values('52354','52', 'IMUES');
insert into sis_ciudad(id, id_depto, nombre) values('52356','52', 'IPIALES');
insert into sis_ciudad(id, id_depto, nombre) values('52378','52', 'LA CRUZ');
insert into sis_ciudad(id, id_depto, nombre) values('52381','52', 'LA FLORIDA');
insert into sis_ciudad(id, id_depto, nombre) values('52385','52', 'LA LLANADA');
insert into sis_ciudad(id, id_depto, nombre) values('52390','52', 'LA TOLA');
insert into sis_ciudad(id, id_depto, nombre) values('52399','52', 'LA UNION');
insert into sis_ciudad(id, id_depto, nombre) values('52405','52', 'LEIVA');
insert into sis_ciudad(id, id_depto, nombre) values('52411','52', 'LINARES');
insert into sis_ciudad(id, id_depto, nombre) values('52418','52', 'LOS ANDES');
insert into sis_ciudad(id, id_depto, nombre) values('52427','52', 'MAGsI');
insert into sis_ciudad(id, id_depto, nombre) values('52435','52', 'MALLAMA');
insert into sis_ciudad(id, id_depto, nombre) values('52473','52', 'MOSQUERA');
insert into sis_ciudad(id, id_depto, nombre) values('52480','52', 'NARIÑO');
insert into sis_ciudad(id, id_depto, nombre) values('52490','52', 'OLAYA HERRERA');
insert into sis_ciudad(id, id_depto, nombre) values('52506','52', 'OSPINA');
insert into sis_ciudad(id, id_depto, nombre) values('52520','52', 'FRANCISCO PIZARRO');
insert into sis_ciudad(id, id_depto, nombre) values('52540','52', 'POLICARPA');
insert into sis_ciudad(id, id_depto, nombre) values('52560','52', 'POTOSI');
insert into sis_ciudad(id, id_depto, nombre) values('52565','52', 'PROVIDENCIA');
insert into sis_ciudad(id, id_depto, nombre) values('52573','52', 'PUERRES');
insert into sis_ciudad(id, id_depto, nombre) values('52585','52', 'PUPIALES');
insert into sis_ciudad(id, id_depto, nombre) values('52612','52', 'RICAURTE');
insert into sis_ciudad(id, id_depto, nombre) values('52621','52', 'ROBERTO PAYAN');
insert into sis_ciudad(id, id_depto, nombre) values('52678','52', 'SAMANIEGO');
insert into sis_ciudad(id, id_depto, nombre) values('52683','52', 'SANDONA');
insert into sis_ciudad(id, id_depto, nombre) values('52685','52', 'SAN BERNARDO');
insert into sis_ciudad(id, id_depto, nombre) values('52687','52', 'SAN LORENZO');
insert into sis_ciudad(id, id_depto, nombre) values('52693','52', 'SAN PABLO');
insert into sis_ciudad(id, id_depto, nombre) values('52694','52', 'SAN PEDRO DE CARTAGO');
insert into sis_ciudad(id, id_depto, nombre) values('52696','52', 'SANTA BARBARA');
insert into sis_ciudad(id, id_depto, nombre) values('52699','52', 'SANTACRUZ');
insert into sis_ciudad(id, id_depto, nombre) values('52720','52', 'SAPUYES');
insert into sis_ciudad(id, id_depto, nombre) values('52786','52', 'TAMINANGO');
insert into sis_ciudad(id, id_depto, nombre) values('52788','52', 'TANGUA');
insert into sis_ciudad(id, id_depto, nombre) values('52835','52', 'SAN ANDRES DE TUMACO');
insert into sis_ciudad(id, id_depto, nombre) values('52838','52', 'TUQUERRES');
insert into sis_ciudad(id, id_depto, nombre) values('52885','52', 'YACUANQUER');
insert into sis_ciudad(id, id_depto, nombre) values('54001','54', 'CUCUTA');
insert into sis_ciudad(id, id_depto, nombre) values('54003','54', 'ABREGO');
insert into sis_ciudad(id, id_depto, nombre) values('54051','54', 'ARBOLEDAS');
insert into sis_ciudad(id, id_depto, nombre) values('54099','54', 'BOCHALEMA');
insert into sis_ciudad(id, id_depto, nombre) values('54109','54', 'BUCARASICA');
insert into sis_ciudad(id, id_depto, nombre) values('54125','54', 'CACOTA');
insert into sis_ciudad(id, id_depto, nombre) values('54128','54', 'CACHIRA');
insert into sis_ciudad(id, id_depto, nombre) values('54172','54', 'CHINACOTA');
insert into sis_ciudad(id, id_depto, nombre) values('54174','54', 'CHITAGA');
insert into sis_ciudad(id, id_depto, nombre) values('54206','54', 'CONVENCION');
insert into sis_ciudad(id, id_depto, nombre) values('54223','54', 'CUCUTILLA');
insert into sis_ciudad(id, id_depto, nombre) values('54239','54', 'DURANIA');
insert into sis_ciudad(id, id_depto, nombre) values('54245','54', 'EL CARMEN');
insert into sis_ciudad(id, id_depto, nombre) values('54250','54', 'EL TARRA');
insert into sis_ciudad(id, id_depto, nombre) values('54261','54', 'EL ZULIA');
insert into sis_ciudad(id, id_depto, nombre) values('54313','54', 'GRAMALOTE');
insert into sis_ciudad(id, id_depto, nombre) values('54344','54', 'HACARI');
insert into sis_ciudad(id, id_depto, nombre) values('54347','54', 'HERRAN');
insert into sis_ciudad(id, id_depto, nombre) values('54377','54', 'LABATECA');
insert into sis_ciudad(id, id_depto, nombre) values('54385','54', 'LA ESPERANZA');
insert into sis_ciudad(id, id_depto, nombre) values('54398','54', 'LA PLAYA');
insert into sis_ciudad(id, id_depto, nombre) values('54405','54', 'LOS PATIOS');
insert into sis_ciudad(id, id_depto, nombre) values('54418','54', 'LOURDES');
insert into sis_ciudad(id, id_depto, nombre) values('54480','54', 'MUTISCUA');
insert into sis_ciudad(id, id_depto, nombre) values('54498','54', 'OCAÑA');
insert into sis_ciudad(id, id_depto, nombre) values('54518','54', 'PAMPLONA');
insert into sis_ciudad(id, id_depto, nombre) values('54520','54', 'PAMPLONITA');
insert into sis_ciudad(id, id_depto, nombre) values('54553','54', 'PUERTO SANTANDER');
insert into sis_ciudad(id, id_depto, nombre) values('54599','54', 'RAGONVALIA');
insert into sis_ciudad(id, id_depto, nombre) values('54660','54', 'SALAZAR');
insert into sis_ciudad(id, id_depto, nombre) values('54670','54', 'SAN CALIXTO');
insert into sis_ciudad(id, id_depto, nombre) values('54673','54', 'SAN CAYETANO');
insert into sis_ciudad(id, id_depto, nombre) values('54680','54', 'SANTIAGO');
insert into sis_ciudad(id, id_depto, nombre) values('54720','54', 'SARDINATA');
insert into sis_ciudad(id, id_depto, nombre) values('54743','54', 'SILOS');
insert into sis_ciudad(id, id_depto, nombre) values('54800','54', 'TEORAMA');
insert into sis_ciudad(id, id_depto, nombre) values('54810','54', 'TIBU');
insert into sis_ciudad(id, id_depto, nombre) values('54820','54', 'TOLEDO');
insert into sis_ciudad(id, id_depto, nombre) values('54871','54', 'VILLA CARO');
insert into sis_ciudad(id, id_depto, nombre) values('54874','54', 'VILLA DEL ROSARIO');
insert into sis_ciudad(id, id_depto, nombre) values('63001','63', 'ARMENIA');
insert into sis_ciudad(id, id_depto, nombre) values('63111','63', 'BUENAVISTA');
insert into sis_ciudad(id, id_depto, nombre) values('63130','63', 'CALARCA');
insert into sis_ciudad(id, id_depto, nombre) values('63190','63', 'CIRCASIA');
insert into sis_ciudad(id, id_depto, nombre) values('63212','63', 'CORDOBA');
insert into sis_ciudad(id, id_depto, nombre) values('63272','63', 'FILANDIA');
insert into sis_ciudad(id, id_depto, nombre) values('63302','63', 'GENOVA');
insert into sis_ciudad(id, id_depto, nombre) values('63401','63', 'LA TEBAIDA');
insert into sis_ciudad(id, id_depto, nombre) values('63470','63', 'MONTENEGRO');
insert into sis_ciudad(id, id_depto, nombre) values('63548','63', 'PIJAO');
insert into sis_ciudad(id, id_depto, nombre) values('63594','63', 'QUIMBAYA');
insert into sis_ciudad(id, id_depto, nombre) values('63690','63', 'SALENTO');
insert into sis_ciudad(id, id_depto, nombre) values('66001','66', 'PEREIRA');
insert into sis_ciudad(id, id_depto, nombre) values('66045','66', 'APIA');
insert into sis_ciudad(id, id_depto, nombre) values('66075','66', 'BALBOA');
insert into sis_ciudad(id, id_depto, nombre) values('66088','66', 'BELEN DE UMBRIA');
insert into sis_ciudad(id, id_depto, nombre) values('66170','66', 'DOSQUEBRADAS');
insert into sis_ciudad(id, id_depto, nombre) values('66318','66', 'GUATICA');
insert into sis_ciudad(id, id_depto, nombre) values('66383','66', 'LA CELIA');
insert into sis_ciudad(id, id_depto, nombre) values('66400','66', 'LA VIRGINIA');
insert into sis_ciudad(id, id_depto, nombre) values('66440','66', 'MARSELLA');
insert into sis_ciudad(id, id_depto, nombre) values('66456','66', 'MISTRATO');
insert into sis_ciudad(id, id_depto, nombre) values('66572','66', 'PUEBLO RICO');
insert into sis_ciudad(id, id_depto, nombre) values('66594','66', 'QUINCHIA');
insert into sis_ciudad(id, id_depto, nombre) values('66682','66', 'SANTA ROSA DE CABAL');
insert into sis_ciudad(id, id_depto, nombre) values('66687','66', 'SANTUARIO');
insert into sis_ciudad(id, id_depto, nombre) values('68001','68', 'BUCARAMANGA');
insert into sis_ciudad(id, id_depto, nombre) values('68013','68', 'AGUADA');
insert into sis_ciudad(id, id_depto, nombre) values('68020','68', 'ALBANIA');
insert into sis_ciudad(id, id_depto, nombre) values('68051','68', 'ARATOCA');
insert into sis_ciudad(id, id_depto, nombre) values('68077','68', 'BARBOSA');
insert into sis_ciudad(id, id_depto, nombre) values('68079','68', 'BARICHARA');
insert into sis_ciudad(id, id_depto, nombre) values('68081','68', 'BARRANCABERMEJA');
insert into sis_ciudad(id, id_depto, nombre) values('68092','68', 'BETULIA');
insert into sis_ciudad(id, id_depto, nombre) values('68101','68', 'BOLIVAR');
insert into sis_ciudad(id, id_depto, nombre) values('68121','68', 'CABRERA');
insert into sis_ciudad(id, id_depto, nombre) values('68132','68', 'CALIFORNIA');
insert into sis_ciudad(id, id_depto, nombre) values('68147','68', 'CAPITANEJO');
insert into sis_ciudad(id, id_depto, nombre) values('68152','68', 'CARCASI');
insert into sis_ciudad(id, id_depto, nombre) values('68160','68', 'CEPITA');
insert into sis_ciudad(id, id_depto, nombre) values('68162','68', 'CERRITO');
insert into sis_ciudad(id, id_depto, nombre) values('68167','68', 'CHARALA');
insert into sis_ciudad(id, id_depto, nombre) values('68169','68', 'CHARTA');
insert into sis_ciudad(id, id_depto, nombre) values('68176','68', 'CHIMA');
insert into sis_ciudad(id, id_depto, nombre) values('68179','68', 'CHIPATA');
insert into sis_ciudad(id, id_depto, nombre) values('68190','68', 'CIMITARRA');
insert into sis_ciudad(id, id_depto, nombre) values('68207','68', 'CONCEPCION');
insert into sis_ciudad(id, id_depto, nombre) values('68209','68', 'CONFINES');
insert into sis_ciudad(id, id_depto, nombre) values('68211','68', 'CONTRATACION');
insert into sis_ciudad(id, id_depto, nombre) values('68217','68', 'COROMORO');
insert into sis_ciudad(id, id_depto, nombre) values('68229','68', 'CURITI');
insert into sis_ciudad(id, id_depto, nombre) values('68235','68', 'EL CARMEN DE CHUCURI');
insert into sis_ciudad(id, id_depto, nombre) values('68245','68', 'EL GUACAMAYO');
insert into sis_ciudad(id, id_depto, nombre) values('68250','68', 'EL PEÑON');
insert into sis_ciudad(id, id_depto, nombre) values('68255','68', 'EL PLAYON');
insert into sis_ciudad(id, id_depto, nombre) values('68264','68', 'ENCINO');
insert into sis_ciudad(id, id_depto, nombre) values('68266','68', 'ENCISO');
insert into sis_ciudad(id, id_depto, nombre) values('68271','68', 'FLORIAN');
insert into sis_ciudad(id, id_depto, nombre) values('68276','68', 'FLORIDABLANCA');
insert into sis_ciudad(id, id_depto, nombre) values('68296','68', 'GALAN');
insert into sis_ciudad(id, id_depto, nombre) values('68298','68', 'GAMBITA');
insert into sis_ciudad(id, id_depto, nombre) values('68307','68', 'GIRON');
insert into sis_ciudad(id, id_depto, nombre) values('68318','68', 'GUACA');
insert into sis_ciudad(id, id_depto, nombre) values('68320','68', 'GUADALUPE');
insert into sis_ciudad(id, id_depto, nombre) values('68322','68', 'GUAPOTA');
insert into sis_ciudad(id, id_depto, nombre) values('68324','68', 'GUAVATA');
insert into sis_ciudad(id, id_depto, nombre) values('68327','68', 'GsEPSA');
insert into sis_ciudad(id, id_depto, nombre) values('68344','68', 'HATO');
insert into sis_ciudad(id, id_depto, nombre) values('68368','68', 'JESUS MARIA');
insert into sis_ciudad(id, id_depto, nombre) values('68370','68', 'JORDAN');
insert into sis_ciudad(id, id_depto, nombre) values('68377','68', 'LA BELLEZA');
insert into sis_ciudad(id, id_depto, nombre) values('68385','68', 'LANDAZURI');
insert into sis_ciudad(id, id_depto, nombre) values('68397','68', 'LA PAZ');
insert into sis_ciudad(id, id_depto, nombre) values('68406','68', 'LEBRIJA');
insert into sis_ciudad(id, id_depto, nombre) values('68418','68', 'LOS SANTOS');
insert into sis_ciudad(id, id_depto, nombre) values('68425','68', 'MACARAVITA');
insert into sis_ciudad(id, id_depto, nombre) values('68432','68', 'MALAGA');
insert into sis_ciudad(id, id_depto, nombre) values('68444','68', 'MATANZA');
insert into sis_ciudad(id, id_depto, nombre) values('68464','68', 'MOGOTES');
insert into sis_ciudad(id, id_depto, nombre) values('68468','68', 'MOLAGAVITA');
insert into sis_ciudad(id, id_depto, nombre) values('68498','68', 'OCAMONTE');
insert into sis_ciudad(id, id_depto, nombre) values('68500','68', 'OIBA');
insert into sis_ciudad(id, id_depto, nombre) values('68502','68', 'ONZAGA');
insert into sis_ciudad(id, id_depto, nombre) values('68522','68', 'PALMAR');
insert into sis_ciudad(id, id_depto, nombre) values('68524','68', 'PALMAS DEL SOCORRO');
insert into sis_ciudad(id, id_depto, nombre) values('68533','68', 'PARAMO');
insert into sis_ciudad(id, id_depto, nombre) values('68547','68', 'PIEDECUESTA');
insert into sis_ciudad(id, id_depto, nombre) values('68549','68', 'PINCHOTE');
insert into sis_ciudad(id, id_depto, nombre) values('68572','68', 'PUENTE NACIONAL');
insert into sis_ciudad(id, id_depto, nombre) values('68573','68', 'PUERTO PARRA');
insert into sis_ciudad(id, id_depto, nombre) values('68575','68', 'PUERTO WILCHES');
insert into sis_ciudad(id, id_depto, nombre) values('68615','68', 'RIONEGRO');
insert into sis_ciudad(id, id_depto, nombre) values('68655','68', 'SABANA DE TORRES');
insert into sis_ciudad(id, id_depto, nombre) values('68669','68', 'SAN ANDRES');
insert into sis_ciudad(id, id_depto, nombre) values('68673','68', 'SAN BENITO');
insert into sis_ciudad(id, id_depto, nombre) values('68679','68', 'SAN GIL');
insert into sis_ciudad(id, id_depto, nombre) values('68682','68', 'SAN JOAQUIN');
insert into sis_ciudad(id, id_depto, nombre) values('68684','68', 'SAN JOSE DE MIRANDA');
insert into sis_ciudad(id, id_depto, nombre) values('68686','68', 'SAN MIGUEL');
insert into sis_ciudad(id, id_depto, nombre) values('68689','68', 'SAN VICENTE DE CHUCURI');
insert into sis_ciudad(id, id_depto, nombre) values('68705','68', 'SANTA BARBARA');
insert into sis_ciudad(id, id_depto, nombre) values('68720','68', 'SANTA HELENA DEL OPON');
insert into sis_ciudad(id, id_depto, nombre) values('68745','68', 'SIMACOTA');
insert into sis_ciudad(id, id_depto, nombre) values('68755','68', 'SOCORRO');
insert into sis_ciudad(id, id_depto, nombre) values('68770','68', 'SUAITA');
insert into sis_ciudad(id, id_depto, nombre) values('68773','68', 'SUCRE');
insert into sis_ciudad(id, id_depto, nombre) values('68780','68', 'SURATA');
insert into sis_ciudad(id, id_depto, nombre) values('68820','68', 'TONA');
insert into sis_ciudad(id, id_depto, nombre) values('68855','68', 'VALLE DE SAN JOSE');
insert into sis_ciudad(id, id_depto, nombre) values('68861','68', 'VELEZ');
insert into sis_ciudad(id, id_depto, nombre) values('68867','68', 'VETAS');
insert into sis_ciudad(id, id_depto, nombre) values('68872','68', 'VILLANUEVA');
insert into sis_ciudad(id, id_depto, nombre) values('68895','68', 'ZAPATOCA');
insert into sis_ciudad(id, id_depto, nombre) values('70001','70', 'SINCELEJO');
insert into sis_ciudad(id, id_depto, nombre) values('70110','70', 'BUENAVISTA');
insert into sis_ciudad(id, id_depto, nombre) values('70124','70', 'CAIMITO');
insert into sis_ciudad(id, id_depto, nombre) values('70204','70', 'COLOSO');
insert into sis_ciudad(id, id_depto, nombre) values('70215','70', 'COROZAL');
insert into sis_ciudad(id, id_depto, nombre) values('70221','70', 'COVEÑAS');
insert into sis_ciudad(id, id_depto, nombre) values('70230','70', 'CHALAN');
insert into sis_ciudad(id, id_depto, nombre) values('70233','70', 'EL ROBLE');
insert into sis_ciudad(id, id_depto, nombre) values('70235','70', 'GALERAS');
insert into sis_ciudad(id, id_depto, nombre) values('70265','70', 'GUARANDA');
insert into sis_ciudad(id, id_depto, nombre) values('70400','70', 'LA UNION');
insert into sis_ciudad(id, id_depto, nombre) values('70418','70', 'LOS PALMITOS');
insert into sis_ciudad(id, id_depto, nombre) values('70429','70', 'MAJAGUAL');
insert into sis_ciudad(id, id_depto, nombre) values('70473','70', 'MORROA');
insert into sis_ciudad(id, id_depto, nombre) values('70508','70', 'OVEJAS');
insert into sis_ciudad(id, id_depto, nombre) values('70523','70', 'PALMITO');
insert into sis_ciudad(id, id_depto, nombre) values('70670','70', 'SAMPUES');
insert into sis_ciudad(id, id_depto, nombre) values('70678','70', 'SAN BENITO ABAD');
insert into sis_ciudad(id, id_depto, nombre) values('70702','70', 'SAN JUAN DE BETULIA');
insert into sis_ciudad(id, id_depto, nombre) values('70708','70', 'SAN MARCOS');
insert into sis_ciudad(id, id_depto, nombre) values('70713','70', 'SAN ONOFRE');
insert into sis_ciudad(id, id_depto, nombre) values('70717','70', 'SAN PEDRO');
insert into sis_ciudad(id, id_depto, nombre) values('70742','70', 'SAN LUIS DE SINCE');
insert into sis_ciudad(id, id_depto, nombre) values('70771','70', 'SUCRE');
insert into sis_ciudad(id, id_depto, nombre) values('70820','70', 'SANTIAGO DE TOLU');
insert into sis_ciudad(id, id_depto, nombre) values('70823','70', 'TOLU VIEJO');
insert into sis_ciudad(id, id_depto, nombre) values('73001','73', 'IBAGUE');
insert into sis_ciudad(id, id_depto, nombre) values('73024','73', 'ALPUJARRA');
insert into sis_ciudad(id, id_depto, nombre) values('73026','73', 'ALVARADO');
insert into sis_ciudad(id, id_depto, nombre) values('73030','73', 'AMBALEMA');
insert into sis_ciudad(id, id_depto, nombre) values('73043','73', 'ANZOATEGUI');
insert into sis_ciudad(id, id_depto, nombre) values('73055','73', 'ARMERO');
insert into sis_ciudad(id, id_depto, nombre) values('73067','73', 'ATACO');
insert into sis_ciudad(id, id_depto, nombre) values('73124','73', 'CAJAMARCA');
insert into sis_ciudad(id, id_depto, nombre) values('73148','73', 'CARMEN DE APICALA');
insert into sis_ciudad(id, id_depto, nombre) values('73152','73', 'CASABIANCA');
insert into sis_ciudad(id, id_depto, nombre) values('73168','73', 'CHAPARRAL');
insert into sis_ciudad(id, id_depto, nombre) values('73200','73', 'COELLO');
insert into sis_ciudad(id, id_depto, nombre) values('73217','73', 'COYAIMA');
insert into sis_ciudad(id, id_depto, nombre) values('73226','73', 'CUNDAY');
insert into sis_ciudad(id, id_depto, nombre) values('73236','73', 'DOLORES');
insert into sis_ciudad(id, id_depto, nombre) values('73268','73', 'ESPINAL');
insert into sis_ciudad(id, id_depto, nombre) values('73270','73', 'FALAN');
insert into sis_ciudad(id, id_depto, nombre) values('73275','73', 'FLANDES');
insert into sis_ciudad(id, id_depto, nombre) values('73283','73', 'FRESNO');
insert into sis_ciudad(id, id_depto, nombre) values('73319','73', 'GUAMO');
insert into sis_ciudad(id, id_depto, nombre) values('73347','73', 'HERVEO');
insert into sis_ciudad(id, id_depto, nombre) values('73349','73', 'HONDA');
insert into sis_ciudad(id, id_depto, nombre) values('73352','73', 'ICONONZO');
insert into sis_ciudad(id, id_depto, nombre) values('73408','73', 'LERIDA');
insert into sis_ciudad(id, id_depto, nombre) values('73411','73', 'LIBANO');
insert into sis_ciudad(id, id_depto, nombre) values('73443','73', 'MARIQUITA');
insert into sis_ciudad(id, id_depto, nombre) values('73449','73', 'MELGAR');
insert into sis_ciudad(id, id_depto, nombre) values('73461','73', 'MURILLO');
insert into sis_ciudad(id, id_depto, nombre) values('73483','73', 'NATAGAIMA');
insert into sis_ciudad(id, id_depto, nombre) values('73504','73', 'ORTEGA');
insert into sis_ciudad(id, id_depto, nombre) values('73520','73', 'PALOCABILDO');
insert into sis_ciudad(id, id_depto, nombre) values('73547','73', 'PIEDRAS');
insert into sis_ciudad(id, id_depto, nombre) values('73555','73', 'PLANADAS');
insert into sis_ciudad(id, id_depto, nombre) values('73563','73', 'PRADO');
insert into sis_ciudad(id, id_depto, nombre) values('73585','73', 'PURIFICACION');
insert into sis_ciudad(id, id_depto, nombre) values('73616','73', 'RIOBLANCO');
insert into sis_ciudad(id, id_depto, nombre) values('73622','73', 'RONCESVALLES');
insert into sis_ciudad(id, id_depto, nombre) values('73624','73', 'ROVIRA');
insert into sis_ciudad(id, id_depto, nombre) values('73671','73', 'SALDAÑA');
insert into sis_ciudad(id, id_depto, nombre) values('73675','73', 'SAN ANTONIO');
insert into sis_ciudad(id, id_depto, nombre) values('73678','73', 'SAN LUIS');
insert into sis_ciudad(id, id_depto, nombre) values('73686','73', 'SANTA ISABEL');
insert into sis_ciudad(id, id_depto, nombre) values('73770','73', 'SUAREZ');
insert into sis_ciudad(id, id_depto, nombre) values('73854','73', 'VALLE DE SAN JUAN');
insert into sis_ciudad(id, id_depto, nombre) values('73861','73', 'VENADILLO');
insert into sis_ciudad(id, id_depto, nombre) values('73870','73', 'VILLAHERMOSA');
insert into sis_ciudad(id, id_depto, nombre) values('73873','73', 'VILLARRICA');
insert into sis_ciudad(id, id_depto, nombre) values('76001','76', 'CALI');
insert into sis_ciudad(id, id_depto, nombre) values('76020','76', 'ALCALA');
insert into sis_ciudad(id, id_depto, nombre) values('76036','76', 'ANDALUCIA');
insert into sis_ciudad(id, id_depto, nombre) values('76041','76', 'ANSERMANUEVO');
insert into sis_ciudad(id, id_depto, nombre) values('76054','76', 'ARGELIA');
insert into sis_ciudad(id, id_depto, nombre) values('76100','76', 'BOLIVAR');
insert into sis_ciudad(id, id_depto, nombre) values('76109','76', 'BUENAVENTURA');
insert into sis_ciudad(id, id_depto, nombre) values('76111','76', 'GUADALAJARA DE BUGA');
insert into sis_ciudad(id, id_depto, nombre) values('76113','76', 'BUGALAGRANDE');
insert into sis_ciudad(id, id_depto, nombre) values('76122','76', 'CAICEDONIA');
insert into sis_ciudad(id, id_depto, nombre) values('76126','76', 'CALIMA');
insert into sis_ciudad(id, id_depto, nombre) values('76130','76', 'CANDELARIA');
insert into sis_ciudad(id, id_depto, nombre) values('76147','76', 'CARTAGO');
insert into sis_ciudad(id, id_depto, nombre) values('76233','76', 'DAGUA');
insert into sis_ciudad(id, id_depto, nombre) values('76243','76', 'EL AGUILA');
insert into sis_ciudad(id, id_depto, nombre) values('76246','76', 'EL CAIRO');
insert into sis_ciudad(id, id_depto, nombre) values('76248','76', 'EL CERRITO');
insert into sis_ciudad(id, id_depto, nombre) values('76250','76', 'EL DOVIO');
insert into sis_ciudad(id, id_depto, nombre) values('76275','76', 'FLORIDA');
insert into sis_ciudad(id, id_depto, nombre) values('76306','76', 'GINEBRA');
insert into sis_ciudad(id, id_depto, nombre) values('76318','76', 'GUACARI');
insert into sis_ciudad(id, id_depto, nombre) values('76364','76', 'JAMUNDI');
insert into sis_ciudad(id, id_depto, nombre) values('76377','76', 'LA CUMBRE');
insert into sis_ciudad(id, id_depto, nombre) values('76400','76', 'LA UNION');
insert into sis_ciudad(id, id_depto, nombre) values('76403','76', 'LA VICTORIA');
insert into sis_ciudad(id, id_depto, nombre) values('76497','76', 'OBANDO');
insert into sis_ciudad(id, id_depto, nombre) values('76520','76', 'PALMIRA');
insert into sis_ciudad(id, id_depto, nombre) values('76563','76', 'PRADERA');
insert into sis_ciudad(id, id_depto, nombre) values('76606','76', 'RESTREPO');
insert into sis_ciudad(id, id_depto, nombre) values('76616','76', 'RIOFRIO');
insert into sis_ciudad(id, id_depto, nombre) values('76622','76', 'ROLDANILLO');
insert into sis_ciudad(id, id_depto, nombre) values('76670','76', 'SAN PEDRO');
insert into sis_ciudad(id, id_depto, nombre) values('76736','76', 'SEVILLA');
insert into sis_ciudad(id, id_depto, nombre) values('76823','76', 'TORO');
insert into sis_ciudad(id, id_depto, nombre) values('76828','76', 'TRUJILLO');
insert into sis_ciudad(id, id_depto, nombre) values('76834','76', 'TULUA');
insert into sis_ciudad(id, id_depto, nombre) values('76845','76', 'ULLOA');
insert into sis_ciudad(id, id_depto, nombre) values('76863','76', 'VERSALLES');
insert into sis_ciudad(id, id_depto, nombre) values('76869','76', 'VIJES');
insert into sis_ciudad(id, id_depto, nombre) values('76890','76', 'YOTOCO');
insert into sis_ciudad(id, id_depto, nombre) values('76892','76', 'YUMBO');
insert into sis_ciudad(id, id_depto, nombre) values('76895','76', 'ZARZAL');
insert into sis_ciudad(id, id_depto, nombre) values('81001','81', 'ARAUCA');
insert into sis_ciudad(id, id_depto, nombre) values('81065','81', 'ARAUQUITA');
insert into sis_ciudad(id, id_depto, nombre) values('81220','81', 'CRAVO NORTE');
insert into sis_ciudad(id, id_depto, nombre) values('81300','81', 'FORTUL');
insert into sis_ciudad(id, id_depto, nombre) values('81591','81', 'PUERTO RONDON');
insert into sis_ciudad(id, id_depto, nombre) values('81736','81', 'SARAVENA');
insert into sis_ciudad(id, id_depto, nombre) values('81794','81', 'TAME');
insert into sis_ciudad(id, id_depto, nombre) values('85001','85', 'YOPAL');
insert into sis_ciudad(id, id_depto, nombre) values('85010','85', 'AGUAZUL');
insert into sis_ciudad(id, id_depto, nombre) values('85015','85', 'CHAMEZA');
insert into sis_ciudad(id, id_depto, nombre) values('85125','85', 'HATO COROZAL');
insert into sis_ciudad(id, id_depto, nombre) values('85136','85', 'LA SALINA');
insert into sis_ciudad(id, id_depto, nombre) values('85139','85', 'MANI');
insert into sis_ciudad(id, id_depto, nombre) values('85162','85', 'MONTERREY');
insert into sis_ciudad(id, id_depto, nombre) values('85225','85', 'NUNCHIA');
insert into sis_ciudad(id, id_depto, nombre) values('85230','85', 'OROCUE');
insert into sis_ciudad(id, id_depto, nombre) values('85250','85', 'PAZ DE ARIPORO');
insert into sis_ciudad(id, id_depto, nombre) values('85263','85', 'PORE');
insert into sis_ciudad(id, id_depto, nombre) values('85279','85', 'RECETOR');
insert into sis_ciudad(id, id_depto, nombre) values('85300','85', 'SABANALARGA');
insert into sis_ciudad(id, id_depto, nombre) values('85315','85', 'SACAMA');
insert into sis_ciudad(id, id_depto, nombre) values('85325','85', 'SAN LUIS DE PALENQUE');
insert into sis_ciudad(id, id_depto, nombre) values('85400','85', 'TAMARA');
insert into sis_ciudad(id, id_depto, nombre) values('85410','85', 'TAURAMENA');
insert into sis_ciudad(id, id_depto, nombre) values('85430','85', 'TRINIDAD');
insert into sis_ciudad(id, id_depto, nombre) values('85440','85', 'VILLANUEVA');
insert into sis_ciudad(id, id_depto, nombre) values('86001','86', 'MOCOA');
insert into sis_ciudad(id, id_depto, nombre) values('86219','86', 'COLON');
insert into sis_ciudad(id, id_depto, nombre) values('86320','86', 'ORITO');
insert into sis_ciudad(id, id_depto, nombre) values('86568','86', 'PUERTO ASIS');
insert into sis_ciudad(id, id_depto, nombre) values('86569','86', 'PUERTO CAICEDO');
insert into sis_ciudad(id, id_depto, nombre) values('86571','86', 'PUERTO GUZMAN');
insert into sis_ciudad(id, id_depto, nombre) values('86573','86', 'LEGUIZAMO');
insert into sis_ciudad(id, id_depto, nombre) values('86749','86', 'SIBUNDOY');
insert into sis_ciudad(id, id_depto, nombre) values('86755','86', 'SAN FRANCISCO');
insert into sis_ciudad(id, id_depto, nombre) values('86757','86', 'SAN MIGUEL');
insert into sis_ciudad(id, id_depto, nombre) values('86760','86', 'SANTIAGO');
insert into sis_ciudad(id, id_depto, nombre) values('86865','86', 'VALLE DEL GUAMUEZ');
insert into sis_ciudad(id, id_depto, nombre) values('86885','86', 'VILLAGARZON');
insert into sis_ciudad(id, id_depto, nombre) values('88001','88', 'SAN ANDRES');
insert into sis_ciudad(id, id_depto, nombre) values('88564','88', 'PROVIDENCIA');
insert into sis_ciudad(id, id_depto, nombre) values('91001','91', 'LETICIA');
insert into sis_ciudad(id, id_depto, nombre) values('91263','91', 'EL ENCANTO');
insert into sis_ciudad(id, id_depto, nombre) values('91405','91', 'LA CHORRERA');
insert into sis_ciudad(id, id_depto, nombre) values('91407','91', 'LA PEDRERA');
insert into sis_ciudad(id, id_depto, nombre) values('91430','91', 'LA VICTORIA');
insert into sis_ciudad(id, id_depto, nombre) values('91460','91', 'MIRITI - PARANA');
insert into sis_ciudad(id, id_depto, nombre) values('91530','91', 'PUERTO ALEGRIA');
insert into sis_ciudad(id, id_depto, nombre) values('91536','91', 'PUERTO ARICA');
insert into sis_ciudad(id, id_depto, nombre) values('91540','91', 'PUERTO NARIÑO');
insert into sis_ciudad(id, id_depto, nombre) values('91669','91', 'PUERTO SANTANDER');
insert into sis_ciudad(id, id_depto, nombre) values('91798','91', 'TARAPACA');
insert into sis_ciudad(id, id_depto, nombre) values('94001','94', 'INIRIDA');
insert into sis_ciudad(id, id_depto, nombre) values('94343','94', 'BARRANCO MINAS');
insert into sis_ciudad(id, id_depto, nombre) values('94663','94', 'MAPIRIPANA');
insert into sis_ciudad(id, id_depto, nombre) values('94883','94', 'SAN FELIPE');
insert into sis_ciudad(id, id_depto, nombre) values('94884','94', 'PUERTO COLOMBIA');
insert into sis_ciudad(id, id_depto, nombre) values('94885','94', 'LA GUADALUPE');
insert into sis_ciudad(id, id_depto, nombre) values('94886','94', 'CACAHUAL');
insert into sis_ciudad(id, id_depto, nombre) values('94887','94', 'PANA PANA');
insert into sis_ciudad(id, id_depto, nombre) values('94888','94', 'MORICHAL');
insert into sis_ciudad(id, id_depto, nombre) values('95001','95', 'SAN JOSE DEL GUAVIARE');
insert into sis_ciudad(id, id_depto, nombre) values('95015','95', 'CALAMAR');
insert into sis_ciudad(id, id_depto, nombre) values('95025','95', 'EL RETORNO');
insert into sis_ciudad(id, id_depto, nombre) values('95200','95', 'MIRAFLORES');
insert into sis_ciudad(id, id_depto, nombre) values('97001','97', 'MITU');
insert into sis_ciudad(id, id_depto, nombre) values('97161','97', 'CARURU');
insert into sis_ciudad(id, id_depto, nombre) values('97511','97', 'PACOA');
insert into sis_ciudad(id, id_depto, nombre) values('97666','97', 'TARAIRA');
insert into sis_ciudad(id, id_depto, nombre) values('97777','97', 'PAPUNAUA');
insert into sis_ciudad(id, id_depto, nombre) values('97889','97', 'YAVARATE');
insert into sis_ciudad(id, id_depto, nombre) values('99001','99', 'PUERTO CARREÑO');
insert into sis_ciudad(id, id_depto, nombre) values('99524','99', 'LA PRIMAVERA');
insert into sis_ciudad(id, id_depto, nombre) values('99624','99', 'SANTA ROSALIA');
insert into sis_ciudad(id, id_depto, nombre) values('99773','99', 'CUMARIBO');


